#!/bin/bash

set -eu

GOLANG_VERSION=1.18
REGISTRY_PREFIX=registry.gitlab.com/orange-opensource/kanod

for var in NEXUS_REGISTRY REPO_URL VERSION
do
    if ! [[ -v "${var}" ]]; then
        echo "${var} must be defined"
        exit 1
    fi
done

export kustomizeVersion="3.8.5"
export CONTAINER=brokerdef
export BROKERDEF_VERSION=${VERSION}

REGISTRY_PREFIX=registry.gitlab.com/orange-opensource/kanod
# shellcheck disable=SC2001
KANOD_MIRROR_REGISTRY="$(sed 's!/.*$!!' <<< "$NEXUS_REGISTRY")"

function kanod_image_sync() {
  echo "- sync of $1"
  for source in $(grep -o 'image: .*' "$1" | tr -d "\"\'" | sed -e 's/^image: //' -e "s/['\"]//g" | sort | uniq); do
    # shellcheck disable=SC2001
    path=$(sed -e 's![^/]*[:.][^/]*/!!' <<< "$source")
    target="${KANOD_MIRROR_REGISTRY}/${path}"
    if [[ "${source}" == "${REGISTRY_PREFIX}/"* ]]; then
        continue
    fi
    if docker manifest inspect "${target}" &> /dev/null; then
        continue
    fi
    # How do we know this is an official image ?
    origin="$(docker image pull -q "${source}")"
    if [[ "$origin" == docker.io/library/* ]]; then
      target="${NEXUS_REGISTRY}/library/${path}"
    fi
    docker image tag "${source}" "${target}"
    docker image push "${target}"
  done
}

declare -a proxy_args
declare -a proxy_run
if [ -n "${http_proxy:-}" ]; then
    proxy_args+=(--build-arg "http_proxy=${http_proxy}")
    proxy_run+=(--env "HTTP_PROXY=${http_proxy}")
fi
if [ -n "${https_proxy:-}" ]; then
    proxy_args+=(--build-arg "https_proxy=${https_proxy}")
    proxy_run+=(--env "HTTPS_PROXY=${https_proxy}")
fi
if [ -n "${no_proxy:-}" ]; then
    proxy_args+=(--build-arg "no_proxy=${no_proxy}")
    proxy_run+=(--env "NO_PROXY=${no_proxy}")
fi
if [ -n "${GOPROXY:-}" ]; then
    proxy_args+=(--build-arg GOPROXY)
    proxy_run+=(--env GOPROXY)
fi


docker run --rm "${proxy_run[@]}" -v "$PWD":/usr/src/myapp -w /usr/src/myapp "golang:${GOLANG_VERSION}" make generate

if [ ! -f kustomize ]; then
    curl -L -o kustomize.tgz https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv${kustomizeVersion}/kustomize_v${kustomizeVersion}_linux_amd64.tar.gz
    tar -zxvf kustomize.tgz kustomize
    rm kustomize.tgz
fi

pushd config/manager && ../../kustomize edit set image "controller=${REGISTRY_PREFIX}/${CONTAINER}:${VERSION}" && popd
./kustomize build config/default -o brokerdef.yaml

echo "Sync images for brokerdef"
# shellcheck disable=SC2016
echo "${NEXUS_KANOD_PASSWORD}" | docker login -u "${NEXUS_KANOD_USER}" --password-stdin "${NEXUS_REGISTRY}"

kanod_image_sync brokerdef.yaml

# bash ./ci-upload.sh

if [ -n "${MANIFEST_ONLY:-}" ]; then
    exit 0
fi

docker run --rm "${proxy_run[@]}" -v "$PWD":/usr/src/myapp -w /usr/src/myapp "golang:${GOLANG_VERSION}" make build

docker build "${proxy_args[@]}" . -t "$NEXUS_REGISTRY/${CONTAINER}:$VERSION"

docker push "${NEXUS_REGISTRY}/${CONTAINER}:${VERSION}"

if [ "${KANOD_PRUNE_IMAGES:-0}" == "1" ]; then
    docker image prune -a --force --filter 'label=project=kanod-brokerdef'
fi
