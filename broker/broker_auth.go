/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package broker

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"

	"github.com/gorilla/mux"
	brokerv1 "gitlab.com/Orange-OpenSource/kanod/brokerdef/api/v1"
	corev1 "k8s.io/api/core/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// BrokerAuth is the type of the implementation of a Redfish broker using ephemeral accounts to virtualize the
// BMC of a server.
type BrokerAuth struct {
	Broker *Broker
}

func (bp *BrokerAuth) Init(b *Broker, r *mux.Router) {
	bp.Broker = b
}

func (bp *BrokerAuth) ServerResponse(bareMetalDef *brokerv1.BareMetalDef) *ServerResponse {
	annotations := bareMetalDef.GetAnnotations()
	serverId := annotations[KanodServernameAnnotation]

	parsedUrl, _ := url.Parse(bareMetalDef.Spec.Url)
	url := parsedUrl.String()

	response := ServerResponse{
		Id:                             serverId,
		BareMetal:                      &bareMetalDef.Name,
		Url:                            &url,
		K8sLabels:                      &bareMetalDef.Spec.K8sLabels,
		K8sAnnotations:                 &bareMetalDef.Spec.K8sAnnotations,
		MacAddress:                     &bareMetalDef.Spec.MacAddress,
		RootDeviceHints:                bareMetalDef.Spec.RootDeviceHints,
		DisableCertificateVerification: &bareMetalDef.Spec.DisableCertificateVerification,
	}
	return &response

}

func (bp *BrokerAuth) BookServer(poolUserName string, poolUserSecret string, server *brokerv1.BareMetalDef) error {
	return bp.CreateAccount(server, poolUserName, poolUserSecret)
}

func (bp *BrokerAuth) ReleaseServer(poolUserName string, server *brokerv1.BareMetalDef) error {
	return bp.RemoveAccount(server, poolUserName)
}

const (
	ACCOUNTS_PATH = "/redfish/v1/AccountService/Accounts"
)

// Account is the go representation of a Redfish account
type Account struct {
	Id       string `json:"Id"`
	Name     string `json:"Name"`
	Password string `json:"Password"`
	UserName string `json:"UserName"`
	RoleId   string `json:"RoleId"`
	Locked   bool   `json:"Locked"`
	Enabled  bool   `json:"Enabled"`
}

// Member is a member of a redfish collection
type Member struct {
	ODataId string `json:"@odata.id"`
}

// RedfishCollection is the type of a collection of records in Redfish.
// It only contains pointers to the actual objects.
type RedfishCollection struct {
	Name    string `json:"Name"`
	Members []Member
}

// RedfishClient is the type of a specialized http.Client for Redfish
// It stores the target host and the credentials.
type RedfishClient struct {
	host     string
	client   *http.Client
	username string
	password string
}

// GetRedfishClient builds a redfish client from a baremetalhost specification.
func (ba *BrokerAuth) GetRedfishClient(bmh *brokerv1.BareMetalDef) (*RedfishClient, error) {
	log := ba.Broker.Log.WithValues("bmh", bmh.Name)
	parsedUrl, err := url.Parse(bmh.Spec.Url)
	if err != nil {
		return nil, err
	}
	secret := corev1.Secret{}
	key := client.ObjectKey{Namespace: ba.Broker.Namespace, Name: bmh.Spec.BareMetalCredential}
	err = ba.Broker.Client.Get(context.Background(), key, &secret)
	if err != nil {
		return nil, err
	}
	userBmc := string(secret.Data["username"])
	passwordBmc := string(secret.Data["password"])

	if userBmc == "" || passwordBmc == "" {
		err := fmt.Errorf("malformed bmc credentials")
		log.Error(err, "Bad credential structure")
		return nil, err
	}
	transport := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	redfishClient := RedfishClient{
		host:     parsedUrl.Host,
		client:   &http.Client{Transport: transport},
		username: string(userBmc),
		password: string(passwordBmc),
	}
	return &redfishClient, nil
}

// DoRequest performs an arbitrary http request using a RedfishClient.
func (c *RedfishClient) DoRequest(method string, path string, body io.Reader) (*http.Response, error) {
	url := fmt.Sprintf("https://%s%s", c.host, path)
	request, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}
	request.Close = true
	request.Header.Set("Content-Type", "application/json;charset=UTF-8")
	request.SetBasicAuth(c.username, c.password)
	return c.client.Do(request)
}

// GetComponents gives back all the paths of a redfish collection.
func (ba *BrokerAuth) GetComponents(client *RedfishClient, target string) ([]string, error) {
	resp, err := client.DoRequest("GET", target, nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("bad status %d", resp.StatusCode)
		ba.Broker.Log.Error(nil, "Get component failed", "path", target)
		return nil, err
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var collection RedfishCollection
	json.Unmarshal(body, &collection)
	paths := make([]string, len(collection.Members))
	for i, member := range collection.Members {
		paths[i] = member.ODataId
	}
	return paths, nil
}

// CreateAccount creates an account on a baremetal host.
// username and password are the credentials of the newly created account.
func (ba *BrokerAuth) CreateAccount(bmh *brokerv1.BareMetalDef, username string, password string) error {
	log := ba.Broker.Log.WithValues("bmh", bmh.Name, "username", username)
	log.Info("Create redfish account")
	redfishClient, err := ba.GetRedfishClient(bmh)
	if err != nil {
		return err
	}
	path, err := ba.FindAccount(redfishClient, username)
	if err != nil {
		return err
	}
	if path == "" {
		body := map[string]string{
			"UserName": username,
			"Password": password,
			"RoleId":   "Operator",
		}
		payload, err := json.Marshal(body)
		if err != nil {
			log.Error(err, "internal error generating json")
			return err
		}
		resp, err := redfishClient.DoRequest("POST", ACCOUNTS_PATH, bytes.NewBuffer(payload))
		if err != nil {
			log.Error(err, "error sending create account request", "path", path)
			return err
		}
		defer resp.Body.Close()
		if resp.StatusCode != http.StatusCreated {
			err := fmt.Errorf("User not created")
			body, _ := io.ReadAll(resp.Body)
			log.Error(err, "Bad status", "path", path, "body", string(body), "status", resp.StatusCode)
			return err
		}
	} else {
		body := map[string]string{
			"Password": password,
		}
		payload, err := json.Marshal(body)
		if err != nil {
			log.Error(err, "internal error generating json")
			return err
		}
		resp, err := redfishClient.DoRequest("PATCH", path, bytes.NewBuffer(payload))
		if err != nil {
			log.Error(err, "error sending patch account request", "path", path)
			return err
		}
		defer resp.Body.Close()
		if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusNoContent {
			err := fmt.Errorf("User not patched")
			body, _ := io.ReadAll(resp.Body)
			log.Error(err, "Bad status", "path", path, "body", string(body), "status", resp.StatusCode)
			return err
		}
	}
	return nil
}

// FindAccount
func (ba *BrokerAuth) FindAccount(redfishClient *RedfishClient, username string) (string, error) {
	// We must first list all the users to find the right id. This is a redfish collection
	paths, err := ba.GetComponents(redfishClient, ACCOUNTS_PATH)
	if err != nil {
		return "", err
	}
	// Iterate on the paths to find the right id
	result := ""
	for _, path := range paths {
		resp, err := redfishClient.DoRequest("GET", path, http.NoBody)
		if err != nil {
			return "", err
		}
		body, err := io.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			return "", err
		}
		if resp.StatusCode != http.StatusOK {
			err := fmt.Errorf("accounts not retrieved")
			ba.Broker.Log.Error(err, "Bad status", "body", string(body))
			return "", err
		}
		var account Account
		json.Unmarshal(body, &account)
		if account.UserName == username {
			result = path
			break
		}
	}
	return result, nil
}

// RemoveAccount removes a redfish account on a baremetal host knowing the username of the
// account.
func (ba *BrokerAuth) RemoveAccount(bmh *brokerv1.BareMetalDef, username string) error {
	log := ba.Broker.Log.WithValues("bmh", bmh.Name, "username", username)
	redfishClient, err := ba.GetRedfishClient(bmh)
	if err != nil {
		return err
	}

	path, err := ba.FindAccount(redfishClient, username)
	if err != nil {
		return err
	}
	if path == "" {
		err := fmt.Errorf("account not found")
		log.Error(err, "Cannot proceed with account deletion")
		return err
	}

	resp, err := redfishClient.DoRequest("DELETE", path, http.NoBody)
	if err != nil {
		log.Error(err, "Sending deletion request failed", "path", path)
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		err := fmt.Errorf("account not deleted")
		body, _ := io.ReadAll(resp.Body)
		log.Error(err, "Bad status", "status", resp.StatusCode, "body", string(body), "path", path)
		return err
	}
	return nil
}
