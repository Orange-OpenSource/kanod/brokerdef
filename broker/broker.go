/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package broker

import (
	"bufio"
	"bytes"
	"context"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"golang.org/x/crypto/bcrypt"
	"golang.org/x/exp/slices"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/util/retry"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"github.com/go-logr/logr"
	"github.com/gorilla/mux"
	bmh "github.com/metal3-io/baremetal-operator/apis/metal3.io/v1alpha1"
	brokerv1 "gitlab.com/Orange-OpenSource/kanod/brokerdef/api/v1"
	corev1 "k8s.io/api/core/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/selection"
)

type BareMetal struct {
	Id                             string               `json:"id"`
	Url                            string               `json:"url"`
	Username                       string               `json:"username"`
	Password                       string               `json:"password"`
	MacAddress                     string               `json:"macAddress"`
	K8sLabels                      map[string]string    `json:"k8slabels"`
	K8sAnnotations                 map[string]string    `json:"k8sannotations"`
	RootDeviceHints                *bmh.RootDeviceHints `json:"rootDeviceHints,omitempty"`
	DisableCertificateVerification bool                 `json:"disablecertificateverification"`
}

type Pool struct {
	Id       string
	Username string
	Servers  map[string]ServerData
}

type User struct {
	Username string
	Password string
}

type InternalStore struct {
	Userstore map[string]*User
	PoolStore map[string]*Pool
}

type PoolLimit struct {
	Label   string
	Value   string
	Max     int
	Current int
}

type ServerData struct {
	Id          string `json:"id"`
	BareMetal   string `json:"baremetal"`
	Schema      string `json:"schema"`
	BmcUsername string `json:"bmcUsername"`
	BmcPassword string `json:"bmcPpassword"`
	Url         string `json:"url"`
}

// BrokerImplem is the inteface that hides the specific implementation of
// the proxy or the authentication back-end that creates accounts.
type BrokerImplem interface {
	// Init initializes the specific implementation with the broker structure
	// and the base router so that new function can be provided.
	Init(b *Broker, r *mux.Router)
	// Synthesize the specific response for a given implementation.
	ServerResponse(bareMetalDef *brokerv1.BareMetalDef) *ServerResponse
	// Operations specific to an implementation performed when baremetal is booked.
	BookServer(poolUserName string, poolUserSecret string, server *brokerv1.BareMetalDef) error
	// Operations specific to an implementation performed when baremetal is released
	ReleaseServer(poolUserName string, server *brokerv1.BareMetalDef) error
}

// Broker implements a broker server that is used as a backend by BareMetalPools to retrieve access to servers
type Broker struct {
	Client       client.Client
	Log          logr.Logger
	Scheme       *runtime.Scheme
	Pools        map[string]*Pool
	Users        map[string]*User
	Mutex        sync.Mutex
	Namespace    string
	BrokerImplem BrokerImplem
}

// PatchSpec is a typed JSON patch description
type PatchSpec struct {
	Op    string `json:"op"`
	Path  string `json:"path"`
	Value string `json:"value"`
}

// ErrorResponseNetDef is the error message for network assocation
type ErrorResponseNetdef struct {
	Code    ErrorCode `json:"code"`
	Message string    `json:"message"`
}

// ErrorCode is the identification of the error
type ErrorCode int

const (
	// KanodPoolnameAnnotation is the annotation containing the poolname value
	KanodPoolnameAnnotation string = "kanod.io/poolname"

	// KanodServernameAnnotation is the annotation containing the servername value
	KanodServernameAnnotation string = "kanod.io/servername"

	// KanodRedfishSchemaAnnotation is the annotation containing the redfish-schema value
	KanodRedfishSchemaAnnotation string = "kanod.io/redfish-schema"

	// kanodBmdefK8sAnnotationPrefix is the prefix for network annotations in K8SAnnotation
	kanodBmdefK8sAnnotationPrefix = "kanod.io/net"

	// KanodBmdefNetworkAnnotation is the annotation containing
	// the network attachement names
	KanodBmdefNetworkAnnotation = "kanod.io/networks"

	// KanodPoolNamelabel is the label containing the pool name value
	KanodPoolNamelabel string = "baremetaldef.kanod.io/poolname"

	// KanodServerNamelabel is the label containing the server name  value
	KanodServerNamelabel string = "baremetaldef.kanod.io/servername"

	// NO_ERROR is given back when network assocation succeeds
	NO_ERROR ErrorCode = iota
	// CHECK_CERTIFICATE_ERROR is given back when certificate is wrong
	CHECK_CERTIFICATE_ERROR
	// OTHER_ERROR is an internal error
	OTHER_ERROR
)

var admin_user string
var admin_password string

func NewHttpClient(Certificate string) *http.Client {
	pool := x509.NewCertPool()
	if Certificate != "" {
		pool.AppendCertsFromPEM([]byte(Certificate))
	}
	tlsConfig := &tls.Config{
		RootCAs: pool,
	}
	transport := &http.Transport{TLSClientConfig: tlsConfig}
	return &http.Client{Transport: transport}
}

func WithHttpsClient(Certificate string) ClientOption {
	return func(c *Client) error {

		c.Client = NewHttpClient(Certificate)
		return nil
	}
}

func AddBasicAuth(username string, password string) RequestEditorFn {
	return func(ctx context.Context, req *http.Request) error {
		req.SetBasicAuth(string(username), string(password))
		return nil
	}
}

func BrokerDefError(w http.ResponseWriter, logger logr.Logger, err error, message string, statusCode int) {
	w.WriteHeader(statusCode)
	logger.Info("Sending back error", "message", message)
	content := Error{Message: message}
	if err := json.NewEncoder(w).Encode(&content); err != nil {
		logger.Error(err, "json encoding error during error reporting")
	}
}

// NewBroker creates the structure associated to a redfish broker server.
func NewBroker(client client.Client, logr logr.Logger, scheme *runtime.Scheme, brokerImplem BrokerImplem) *Broker {
	namespace := os.Getenv("BROKER_NAMESPACE")
	if namespace == "" {
		namespace = "brokerdef-system"
	}
	return &Broker{
		Client:       client,
		Log:          logr,
		Pools:        make(map[string]*Pool),
		Users:        make(map[string]*User),
		Mutex:        sync.Mutex{},
		Scheme:       scheme,
		Namespace:    namespace,
		BrokerImplem: brokerImplem,
	}
}

func (b *Broker) loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var stringBody interface{}
		bodyBuffer, _ := io.ReadAll(r.Body)
		json.Unmarshal([]byte(bodyBuffer), &stringBody)
		b.Log.Info("Broker request", "url", r.RequestURI, "method", r.Method, "host", r.Host)
		reader := io.NopCloser(bytes.NewBuffer(bodyBuffer))
		r.Body = reader
		next.ServeHTTP(w, r)
	})
}

func (b *Broker) authentPool(w http.ResponseWriter, r *http.Request, poolId string) bool {
	authOk := false
	username, password, ok := r.BasicAuth()

	ctxVal := r.Context().Value(PoolBasicAuthScopes).([]string)

	if slices.Contains(ctxVal, "pool") {
		authOk = ok && b.VerifyPoolAuthent(username, password, poolId)
	}

	if !authOk {
		b.Log.Info(fmt.Sprintf("pool authent NOK with poolid %s and username %s \n", poolId, username))
		w.WriteHeader(http.StatusUnauthorized)
		return false
	}
	return true
}

func (b *Broker) authentInfra(w http.ResponseWriter, r *http.Request) bool {
	username, password, _ := r.BasicAuth()
	authOk := false

	ctxVal := r.Context().Value(PoolBasicAuthScopes).([]string)

	if slices.Contains(ctxVal, "infra") {
		if (admin_user == username) && (bcrypt.CompareHashAndPassword([]byte(admin_password), []byte(password))) == nil {
			authOk = true
		}
	}

	if !authOk {
		b.Log.Info(fmt.Sprintf("pool authent NOK with and username %s \n", username))
		w.WriteHeader(http.StatusUnauthorized)
		return false
	}
	return true
}

func (b *Broker) HandleBrokerApiRequests() {

	admin_user = os.Getenv("ADMIN_USERNAME")
	if admin_user == "" {
		err := errors.New("ADMIN_USERNAME undefined")
		b.Log.Error(err, "Environment variable ADMIN_USERNAME should be declared")
	}
	admin_password = os.Getenv("ADMIN_PASSWORD")
	if admin_password == "" {
		err := errors.New("ADMIN_PASSWORD undefined")
		b.Log.Error(err, "Environment variable ADMIN_PASSWORD should be declared")
	}
	broker_address := os.Getenv("BROKER_ADDRESS")
	if broker_address == "" {
		broker_address = ":9000"
	}

	r := mux.NewRouter().StrictSlash(true)

	HandlerFromMux(b, r)

	b.BrokerImplem.Init(b, r)
	r.Use(b.loggingMiddleware)

	err := http.ListenAndServe(broker_address, r)
	if err != nil {
		b.Log.Error(err, "unable to start http server")
	}
}

func (b *Broker) Dump(w http.ResponseWriter, r *http.Request) {
	if !b.authentInfra(w, r) {
		return
	}

	b.Log.Info("Dump broker")
	store := InternalStore{PoolStore: b.Pools, Userstore: b.Users}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(store)
}

func (b *Broker) GetPooluserPassword(w http.ResponseWriter, r *http.Request, poolUserId string) {
	if !b.authentInfra(w, r) {
		return
	}

	b.Log.Info("Get pooluser password", "pooluser", poolUserId)

	poolUser, err := b.GetPoolUser(poolUserId)
	if err != nil {
		BrokerDefError(w, b.Log, err, "cannot get pooluser", http.StatusNotFound)
		return
	}

	if poolUser == nil {
		w.WriteHeader(http.StatusNotFound)
		message := Error{Message: "pooluser not found"}
		json.NewEncoder(w).Encode(&message)
		return
	}

	secret, err := b.GetSecret(poolUser.Spec.CredentialName)
	if err != nil {
		BrokerDefError(w, b.Log, err, "cannot get secret", http.StatusInternalServerError)
		return
	}

	password, ok := secret.Data["password"]

	if !ok {
		BrokerDefError(w, b.Log, err, "cannot get secret for pooluser", http.StatusNotFound)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(PasswordData{Password: string(password)})
}

func (b *Broker) GetPool(w http.ResponseWriter, r *http.Request, poolId string) {
	if !b.authentPool(w, r, poolId) {
		return
	}

	b.Log.Info("Get pool", "pool", poolId)
	poolDef, err := b.getPooldefByName(poolId, b.Namespace)
	if err != nil {
		BrokerDefError(w, b.Log, err, "cannot get pooldef", http.StatusNotFound)
		return
	}

	response := b.GetPoolResponse(poolDef)
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(&response)
}

func (b *Broker) GetServerByID(w http.ResponseWriter, r *http.Request, poolId string, serverId string) {
	if !b.authentPool(w, r, poolId) {
		return
	}

	b.Log.Info("Get server", "pool", poolId, "server", serverId)

	bmdef, err := b.getBaremetaldefInPool(b.Namespace, poolId, serverId)
	if err != nil {
		BrokerDefError(w, b.Log, err, "cannot get server", http.StatusInternalServerError)
		return
	}

	if bmdef == nil {
		BrokerDefError(w, b.Log, errors.New("get server - server not found"), "server id not found", http.StatusNotFound)
		return
	}

	server := b.GetServerResponse(bmdef)

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(server); err != nil {
		BrokerDefError(w, b.Log, err, "Error encoding response object", http.StatusInternalServerError)
		return
	}
}

func (b *Broker) RemoveServer(w http.ResponseWriter, r *http.Request, poolId string, serverId string) {
	if !b.authentPool(w, r, poolId) {
		return
	}

	b.Log.Info("Remove server", "pool", poolId, "server", serverId)
	pool := b.getPoolByIdInPoolStore(poolId)
	if pool == nil {
		BrokerDefError(w, b.Log, errors.New("remove server - pool not found"), "pool not found", http.StatusNotFound)
		return
	}

	pooldef, err := b.getPooldefByName(poolId, b.Namespace)
	if err != nil {
		BrokerDefError(w, b.Log, err, "cannot get pooldef", http.StatusInternalServerError)
		return
	}
	if pooldef == nil {
		BrokerDefError(w, b.Log, err, "pool not found", http.StatusNotFound)
		return
	}

	b.Mutex.Lock()
	defer b.Mutex.Unlock()

	bmdef, err := b.getBaremetaldefInPool(b.Namespace, poolId, serverId)
	if err != nil {
		BrokerDefError(w, b.Log, err, "cannot get baremetaldef", http.StatusInternalServerError)
		return
	}

	if bmdef == nil {
		BrokerDefError(w, b.Log, errors.New("remove server - server not found"), "server id not found", http.StatusNotFound)
	} else {
		baremetalName := bmdef.Name

		bareMetalDef := &brokerv1.BareMetalDef{}
		key := client.ObjectKey{Namespace: b.Namespace, Name: baremetalName}
		err := b.Client.Get(context.Background(), key, bareMetalDef)
		if err != nil {
			BrokerDefError(w, b.Log, err, "cannot release baremetal", http.StatusInternalServerError)
			return
		}

		err = b.BrokerImplem.ReleaseServer(pooldef.Spec.UserName, bareMetalDef)
		if err != nil {
			BrokerDefError(w, b.Log, err, "cannot release baremetal", http.StatusInternalServerError)
			return
		}
		_, err = b.AddLabelsAndAnnotations(baremetalName, b.Namespace, "", "", "")
		if err != nil {
			BrokerDefError(w, b.Log, err, "cannot remove baremetal annotation", http.StatusNotFound)
			return
		}

		b.ReleaseOneServer(pooldef)
		b.DisassociateServerFromPool(serverId, poolId)
		w.WriteHeader(http.StatusOK)
		return

	}
}

func (b *Broker) GetAllServers(w http.ResponseWriter, r *http.Request, poolId string) {
	if !b.authentPool(w, r, poolId) {
		return
	}

	b.Log.Info("Get pool servers", "pool", poolId)
	poolDef, err := b.getPooldefByName(poolId, b.Namespace)
	if err != nil {
		BrokerDefError(w, b.Log, err, "cannot get pooldef", http.StatusNotFound)
		return
	}

	if poolDef == nil {
		BrokerDefError(w, b.Log, errors.New("get pool servers - pool not found"), "pool not found", http.StatusNotFound)
		return
	}

	res := b.RetrieveAllServers(poolId)

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(res); err != nil {
		BrokerDefError(w, b.Log, err, "Error encoding response object", http.StatusInternalServerError)
		return
	}
}

func (b *Broker) CreateServer(w http.ResponseWriter, r *http.Request, poolId string) {
	if !b.authentPool(w, r, poolId) {
		return
	}

	b.Log.Info("Create server", "pool", poolId)

	w.Header().Set("Content-Type", "application/json")
	var postData ServerRequest

	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&postData)

	if err != nil {
		msg := fmt.Sprintf("Error during body request decoding (%s)", err.Error())
		BrokerDefError(w, b.Log, err, msg, http.StatusBadRequest)
		return
	}
	b.Mutex.Lock()
	defer b.Mutex.Unlock()

	err = ValidateServerDataRequest(postData)
	if err != nil {
		msg := fmt.Sprintf("Invalid data for server creation (%s)", err.Error())
		BrokerDefError(w, b.Log, err, msg, http.StatusBadRequest)
		return
	}

	//pool := b.getPoolById(poolId)
	poolDef, err := b.getPooldefByName(poolId, b.Namespace)
	if err != nil {
		BrokerDefError(w, b.Log, err, "cannot get pooldef", http.StatusInternalServerError)
		return
	}
	if poolDef == nil {
		BrokerDefError(w, b.Log, err, "pool not found", http.StatusNotFound)
		return
	}

	poolUser, err := b.GetPoolUser(poolDef.Spec.UserName)
	if err != nil {
		BrokerDefError(w, b.Log, err, "cannot get pooluser for pool", http.StatusNotFound)
		return
	}

	if poolUser == nil {
		BrokerDefError(w, b.Log, err, "pooluser not found", http.StatusNotFound)
		return
	}

	poolsecret, err := b.GetSecret(poolUser.Spec.CredentialName)
	if err != nil {
		BrokerDefError(w, b.Log, err, "cannot get secret for pooluser", http.StatusInternalServerError)
		return
	}

	if poolsecret == nil {
		BrokerDefError(w, b.Log, err, "secret not found", http.StatusNotFound)
		return
	}

	serverId := postData.Id
	schema := postData.Schema

	bmdef, err := b.getBaremetaldefInPool(b.Namespace, poolId, serverId)
	if err != nil {
		BrokerDefError(w, b.Log, err, "cannot get baremetaldef", http.StatusInternalServerError)
		return
	}

	if bmdef != nil {
		BrokerDefError(w, b.Log, err, "server id already exists", http.StatusConflict)
		return
	}

	bmdefInPoolList, err := b.getUsedBaremetaldefList(b.Namespace, poolId)
	if err != nil {
		BrokerDefError(w, b.Log, err, "cannot get baremetaldef", http.StatusInternalServerError)
		return
	}

	if poolDef.Spec.MaxServers >= 0 && len(bmdefInPoolList.Items) >= poolDef.Spec.MaxServers {
		BrokerDefError(w, b.Log, err, "maxServers exceeded", http.StatusForbidden)
		return
	}

	ok, err := b.BookOneServer(poolDef)
	if err != nil {
		BrokerDefError(w, b.Log, err, "cannot book server", http.StatusInternalServerError)
		return
	}

	if !ok {
		BrokerDefError(w, b.Log, err, "user quota exceeded", http.StatusForbidden)
		return
	}

	unusedBmdefList, err := b.getUnusedBaremetaldefList()
	if err != nil {
		BrokerDefError(w, b.Log, err, "Failed to list baremetaldef", http.StatusInternalServerError)
		return
	}

	var selectedBareMetal *brokerv1.BareMetalDef = nil
	for _, baremetaldef := range unusedBmdefList.Items {
		bmOk := true
		for key := range poolDef.Spec.LabelSelector {
			v, ok := baremetaldef.ObjectMeta.Labels[key]
			if !ok {
				bmOk = false
			}

			if ok && (poolDef.Spec.LabelSelector[key] != v) {
				bmOk = false
			}
		}
		if bmOk {
			selectedBareMetal = &baremetaldef
			break
		}
	}

	if selectedBareMetal != nil {
		b.Log.Info(
			"Server Association", "pool", poolId,
			"serverId", serverId, "serverName", selectedBareMetal.Name)
		bmdefUpdated, err := b.AddLabelsAndAnnotations(selectedBareMetal.Name, b.Namespace, poolId, serverId, schema)
		if err != nil {
			err := b.ReleaseOneServer(poolDef)
			BrokerDefError(w, b.Log, err, "cannot add label/annotation on baremetaldef", http.StatusNotFound)
			return
		}

		err = b.BrokerImplem.BookServer(poolDef.Spec.UserName, string(poolsecret.Data["password"]), selectedBareMetal)
		if err != nil {
			b.ReleaseOneServer(poolDef)
			BrokerDefError(w, b.Log, err, "cannot change baremetal state", http.StatusNotFound)
			return
		}
		err = b.AssociateServerToPool(poolId, serverId, *bmdefUpdated, schema)
		if err != nil {
			b.ReleaseOneServer(poolDef)
			BrokerDefError(w, b.Log, err, "cannot create server", http.StatusInternalServerError)
			return
		}

		response := b.GetServerResponse(bmdefUpdated)
		w.WriteHeader(http.StatusCreated)
		err = json.NewEncoder(w).Encode(&response)
		if err != nil {
			b.Log.Error(err, "Cannot encode response")
		} else {
			b.Log.Info("Reply successfully sent", "pool", poolId, "server", serverId)
		}
		return

	} else {
		b.ReleaseOneServer(poolDef)
		BrokerDefError(w, b.Log, errors.New("no baremetal available"), "no baremetal available", http.StatusNotFound)
		return
	}
}

// //////////////////////////////////////////////////////////////
func (b *Broker) VerifyUserAuthent(username string, password string) bool {
	user, ok := b.getUserInUserStore(username)
	return ok && user.Password == password
}

func (b *Broker) VerifyPoolAuthent(username string, password string, poolId string) bool {
	pool, ok := b.Pools[poolId]
	if !ok {
		return false
	}
	return pool.Username == username && b.VerifyUserAuthent(username, password)
}

func (b *Broker) AssociateServerToPool(poolId string, serverId string, bareMetalDef brokerv1.BareMetalDef, schema string) error {
	secret, err := b.GetSecret(bareMetalDef.Spec.BareMetalCredential)
	if err != nil {
		return err
	}

	if !b.PoolFound(poolId) {
		err := errors.New("pool not found in internal database")
		return err
	}

	var serverStored = ServerData{
		Id:          serverId,
		BareMetal:   bareMetalDef.Name,
		Schema:      schema,
		BmcUsername: string(secret.Data["username"]),
		BmcPassword: string(secret.Data["password"]),
		Url:         bareMetalDef.Spec.Url,
	}
	b.Pools[poolId].Servers[serverId] = serverStored

	return nil
}

func (b *Broker) CreateNewPoolInPoolStore(pool Pool) {
	var newPool = Pool{
		Id:       pool.Id,
		Username: pool.Username,
		Servers:  make(map[string]ServerData),
	}
	b.Pools[pool.Id] = &newPool

}

func (b *Broker) RetrieveAllServers(poolId string) []ServerResponse {
	res := []ServerResponse{}

	bmdefList, _ := b.getUsedBaremetaldefList(b.Namespace, poolId)

	for _, baremetaldef := range bmdefList.Items {
		server := b.GetServerResponse(&baremetaldef)
		data, err := json.Marshal(server)
		if err != nil {
			b.Log.Error(err, "Cannot Marshal server struct")
		}

		var elem ServerResponse
		if err := json.Unmarshal(data, &elem); err != nil {
			b.Log.Error(err, "Cannot Unmarshal server struct")
		}

		res = append(res, elem)
	}
	return res
}

func (b *Broker) RetrieveAllPools() ([]PoolResponse, error) {
	res := []PoolResponse{}

	poolList := &brokerv1.PoolDefList{}
	err := b.Client.List(context.Background(), poolList, client.InNamespace(b.Namespace))
	if err != nil {
		return nil, err
	}
	for _, pool := range poolList.Items {
		poolResponse := b.GetPoolResponse(&pool)
		res = append(res, *poolResponse)
	}
	return res, nil
}

func (b *Broker) DeleteBareMetal(baremetalName string, log logr.Logger) error {
	var poolId, serverId string
	for _, pool := range b.Pools {
		for _, server := range pool.Servers {
			if baremetalName == server.BareMetal {
				poolId = pool.Id
				serverId = server.Id

				break
			}
		}
	}

	pooldef, err := b.getPooldefByName(poolId, b.Namespace)
	if err != nil {
		return err
	}
	if pooldef == nil {
		return err
	}

	err = b.ReleaseOneServer(pooldef)
	if err != nil {
		return err
	}

	b.DisassociateServerFromPool(serverId, poolId)
	delete(b.Pools[poolId].Servers, serverId)

	return nil
}

func (b *Broker) GetPoolResponse(pool *brokerv1.PoolDef) *PoolResponse {
	res := PoolResponse{
		Id:            &pool.Name,
		LabelSelector: &pool.Spec.LabelSelector,
		MaxServers:    &pool.Spec.MaxServers,
		Username:      &pool.Spec.UserName,
	}
	return &res

}

func (b *Broker) DeleteUserInPoolStore(userId string, logger logr.Logger) {
	b.Log.Info("delete user", "user", userId)
	for poolId, pool := range b.Pools {
		if pool.Username == userId {
			b.DeletePoolInPoolStore(poolId)
		}
	}
	delete(b.Users, userId)
}

func (b *Broker) DeletePoolInPoolStore(poolId string) {
	b.Log.Info("delete pool", "pool", poolId)

	pool := b.Pools[poolId]
	poolusername := pool.Username

	for serverName := range b.Pools[poolId].Servers {
		baremetalName := b.Pools[poolId].Servers[serverName].BareMetal
		b.AddLabelsAndAnnotations(baremetalName, b.Namespace, "", "", "")
		b.UpdateUserUsage(poolusername, b.Namespace)
		b.DisassociateServerFromPool(serverName, poolId)
	}
	delete(b.Pools, poolId)
}

func (b *Broker) PoolFound(name string) bool {
	_, found := b.Pools[name]
	return found
}

func (b *Broker) UpdatePoolValuesInPoolStore(name string, poolValues Pool) {
	tmp := b.Pools[name]
	tmp.Username = poolValues.Username
	b.Pools[name] = tmp
}

func ValidateServerDataRequest(serverValues ServerRequest) error {
	if len(serverValues.Id) == 0 {
		return errors.New("id required")
	}
	return nil
}

func (b *Broker) ServerFound(name string, poolid string) bool {
	if pool, ok := b.Pools[poolid]; !ok {
		return false
	} else {
		_, found := pool.Servers[name]
		return found
	}
}

func (b *Broker) GetServerResponse(bmdef *brokerv1.BareMetalDef) *ServerResponse {
	return b.BrokerImplem.ServerResponse(bmdef)
}

func (b *Broker) DisassociateServerFromPool(serverId string, poolId string) {
	pool := b.Pools[poolId]
	delete(pool.Servers, serverId)
}

func (b *Broker) getPoolByIdInPoolStore(poolId string) *Pool {
	return b.Pools[poolId]
}

func (b *Broker) AddUserInUserStore(user *User) {
	b.Users[user.Username] = user
}

func (b *Broker) AddServerInPoolStore(poolname string, serverdata *ServerData) {
	b.Pools[poolname].Servers[serverdata.Id] = *serverdata
}

func (b *Broker) getUserInUserStore(username string) (*User, bool) {
	res, ok := b.Users[username]
	return res, ok
}

func (b *Broker) getCredRedfish(poolId string, serverId string) (string, string) {
	server, found := b.Pools[poolId].Servers[serverId]
	if found {
		return server.BmcUsername, server.BmcPassword
	}
	return "", ""

}

func (b *Broker) getRedfishAddr(poolId string, serverId string) string {
	server, found := b.Pools[poolId].Servers[serverId]
	if found {
		parsedUrl, _ := url.Parse(server.Url)
		newUrl := parsedUrl.Scheme + "://" + parsedUrl.Host
		return newUrl
	}
	return ""

}

func (b *Broker) AddLabelsAndAnnotations(baremetalName string, namespace string, poolId string, serverId string, schema string) (*brokerv1.BareMetalDef, error) {
	var err error = nil
	ctx := context.Background()

	resultBareMetalDef := &brokerv1.BareMetalDef{}

	err = retry.RetryOnConflict(retry.DefaultRetry,
		func() error {
			b.Log.Info("Get baremetaldefs resource for updating", "baremetal", baremetalName)
			key := client.ObjectKey{Namespace: namespace, Name: baremetalName}
			err = b.Client.Get(ctx, key, resultBareMetalDef)
			if err != nil {
				b.Log.Error(err, "Failed to get baremetaldef")
				return err
			}

			annotations := resultBareMetalDef.GetAnnotations()
			if annotations == nil {
				annotations = make(map[string]string)
			}
			labels := resultBareMetalDef.GetLabels()
			if labels == nil {
				labels = make(map[string]string)
			}

			annotations[KanodPoolnameAnnotation] = poolId
			annotations[KanodServernameAnnotation] = serverId
			annotations[KanodRedfishSchemaAnnotation] = schema
			resultBareMetalDef.SetAnnotations(annotations)

			if poolId == "" {
				delete(labels, KanodPoolNamelabel)
				delete(labels, KanodServerNamelabel)
				resultBareMetalDef.Spec.PoolDef = ""
			} else {
				labels[KanodPoolNamelabel] = poolId
				labels[KanodServerNamelabel] = serverId
				resultBareMetalDef.Spec.PoolDef = poolId
			}

			resultBareMetalDef.SetLabels(labels)

			if resultBareMetalDef.Spec.K8sAnnotations == nil {
				resultBareMetalDef.Spec.K8sAnnotations = make(map[string]string)
			} else {
				for key := range resultBareMetalDef.Spec.K8sAnnotations {
					if strings.HasPrefix(key, kanodBmdefK8sAnnotationPrefix) {
						delete(resultBareMetalDef.Spec.K8sAnnotations, key)
					}
				}
			}

			b.Log.Info("Updating baremetaldefs resource", "baremetal", baremetalName)
			err = b.Client.Update(ctx, resultBareMetalDef)
			if err != nil {
				return err
			}

			return nil
		})
	if err != nil {
		b.Log.Error(err, "Cannot update annotations/labels on baremetaldefs", "baremetaldef", baremetalName)
		return nil, err
	}

	return resultBareMetalDef, nil

}

func (b *Broker) RemovePool(w http.ResponseWriter, r *http.Request, poolId string) {
	if !b.authentPool(w, r, poolId) {
		return
	}

	b.Log.Info("Remove pool", "pool", poolId)

	pool := b.getPoolByIdInPoolStore(poolId)
	if pool == nil {
		BrokerDefError(w, b.Log, errors.New("remove pool - pool not found"), "pool not found", http.StatusNotFound)
		return
	}

	pooldef, err := b.getPooldefByName(poolId, b.Namespace)
	if err != nil {
		BrokerDefError(w, b.Log, err, "cannot get pooldef", http.StatusInternalServerError)
		return
	}
	if pooldef == nil {
		BrokerDefError(w, b.Log, err, "pool not found", http.StatusNotFound)
		return
	}

	err = b.Client.Delete(context.Background(), pooldef)

	if err != nil {
		BrokerDefError(w, b.Log, err, "cannot delete pooldef", http.StatusInternalServerError)
		return
	}

	for pooldef != nil {
		b.Log.Info("check pool is deleted", "poolname", poolId)
		pooldef, _ = b.getPooldefByName(poolId, b.Namespace)
		time.Sleep(1000 * time.Millisecond)
	}

	w.WriteHeader(http.StatusOK)
}

func (b *Broker) CreatePool(w http.ResponseWriter, r *http.Request) {
	username, password, _ := r.BasicAuth()
	if !b.VerifyUserAuthent(username, password) {
		msg := fmt.Sprintf("create pool - authent failed for user %s", username)
		BrokerDefError(w, b.Log, errors.New(msg), "Unauthorized - invalid credentials", http.StatusUnauthorized)
		return
	}
	b.Log.Info("Create pool")

	b.Mutex.Lock()
	defer b.Mutex.Unlock()

	ctx := context.Background()
	var postData PoolRequest
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&postData)

	if err != nil {
		msg := fmt.Sprintf("Error during body request decoding (%s)", err.Error())
		BrokerDefError(w, b.Log, err, msg, http.StatusBadRequest)
		return
	}

	b.Log.Info("Create pool", "poolname", postData.Id)

	pooldef, err := b.getPooldefByName(*postData.Id, b.Namespace)
	if err != nil {
		BrokerDefError(w, b.Log, err, "cannot get pooldef", http.StatusInternalServerError)
		return
	}
	if pooldef != nil {
		BrokerDefError(w, b.Log, errors.New("create pool - pool exists"), "Pool already exists", http.StatusConflict)
		return
	}

	if postData.LabelSelector == nil {
		labelSelector := make(map[string]string)
		postData.LabelSelector = &labelSelector
	}

	poolDef := &brokerv1.PoolDef{
		TypeMeta: metav1.TypeMeta{
			APIVersion: brokerv1.GroupVersion.String(),
			Kind:       "PoolDef",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      *postData.Id,
			Namespace: b.Namespace,
		},
		Spec: brokerv1.PoolDefSpec{
			UserName:      username,
			MaxServers:    *postData.MaxServers,
			LabelSelector: *postData.LabelSelector,
		},
	}

	err = b.Client.Create(ctx, poolDef)
	if err != nil {
		BrokerDefError(w, b.Log, err, "Internal error during creation", http.StatusInternalServerError)
	}
	w.WriteHeader(http.StatusCreated)
}

func InitializeLimits(limits []brokerv1.PoolLimit) []PoolLimit {
	newLimits := make([]PoolLimit, len(limits))
	for i, limit := range limits {
		newLimits[i] = PoolLimit{
			Label:   limit.Label,
			Value:   limit.Value,
			Max:     limit.Max,
			Current: 0,
		}
	}
	return newLimits
}

func (b *Broker) InitBrokerData(restConfig *rest.Config, setupLog logr.Logger) error {
	var err error = nil
	var poolname, servername string
	var urlSchema = "redfish"
	ctx := context.Background()

	b.Mutex.Lock()
	defer b.Mutex.Unlock()

	dynClient, err := dynamic.NewForConfig(restConfig)
	if err != nil {
		b.Log.Error(err, "Problem while creating client for cluster.")
		return err
	}

	gvrPoolusers := schema.GroupVersionResource{Group: "broker.kanod.io", Version: "v1", Resource: "poolusers"}
	gvrPooldefs := schema.GroupVersionResource{Group: "broker.kanod.io", Version: "v1", Resource: "pooldefs"}
	gvrBaremetaldefs := schema.GroupVersionResource{Group: "broker.kanod.io", Version: "v1", Resource: "baremetaldefs"}
	gvrsecret := schema.GroupVersionResource{Group: "", Version: "v1", Resource: "secrets"}

	pooluserList, err := dynClient.Resource(gvrPoolusers).Namespace(b.Namespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		setupLog.Error(err, "cannot get pooluser list")
		return err
	}

	for _, crd := range pooluserList.Items {
		metadata := crd.Object["metadata"].(map[string]interface{})
		poolUserName := fmt.Sprintf("%v", metadata["name"])

		spec := crd.Object["spec"].(map[string]interface{})
		credentialName := fmt.Sprintf("%v", spec["credentialName"])

		secret, err := dynClient.Resource(gvrsecret).Namespace(b.Namespace).Get(ctx, credentialName, metav1.GetOptions{})
		if err != nil {
			b.Log.Error(err, "error while getting baremetalpool secret in cluster")
			return err
		}
		data := secret.Object["data"].(map[string]interface{})
		encodedPassword := fmt.Sprintf("%v", data["password"])
		password, _ := base64.StdEncoding.DecodeString(encodedPassword)

		var newUser User
		newUser.Username = poolUserName
		newUser.Password = string(password)
		b.AddUserInUserStore(&newUser)

	}

	pooldefList, err := dynClient.Resource(gvrPooldefs).Namespace(b.Namespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		setupLog.Error(err, "cannot get pooldef list")
	}
	for _, crd := range pooldefList.Items {
		metadata := crd.Object["metadata"].(map[string]interface{})
		poolName := fmt.Sprintf("%v", metadata["name"])
		spec := crd.Object["spec"].(map[string]interface{})
		pooluserName := fmt.Sprintf("%v", spec["username"])

		var newPool Pool
		newPool.Id = poolName
		newPool.Username = pooluserName

		poolFound := b.PoolFound(poolName)
		if !poolFound {
			b.CreateNewPoolInPoolStore(newPool)
		}
	}

	baremetaldefList, err := dynClient.Resource(gvrBaremetaldefs).Namespace(b.Namespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		setupLog.Error(err, "cannot get baremetaldef list")
	}

	for _, crd := range baremetaldefList.Items {
		annotations := crd.GetAnnotations()
		if val, ok := annotations[KanodPoolnameAnnotation]; ok {
			poolname = val
		}
		if val, ok := annotations[KanodServernameAnnotation]; ok {
			servername = val
		}
		if val, ok := annotations[KanodRedfishSchemaAnnotation]; ok {
			urlSchema = val
		}

		var bareMetalDef brokerv1.BareMetalDef
		err := runtime.DefaultUnstructuredConverter.FromUnstructured(crd.Object, &bareMetalDef)
		if err != nil {
			setupLog.Error(err, "cannot convert to typed baremetaldef")
		}

		// we use internal pool info it was initialized just before; no need to get PoolDef/PoolUser custom resources again
		pool := b.getPoolByIdInPoolStore(poolname)
		if pool != nil {
			pooluser, ok := b.getUserInUserStore(pool.Username)
			if !ok {
				return err
			}
			err := b.BrokerImplem.BookServer(pool.Username, pooluser.Password, &bareMetalDef)
			if err != nil {
				return err
			}
			secret, err := dynClient.Resource(gvrsecret).Namespace(b.Namespace).Get(ctx, bareMetalDef.Spec.BareMetalCredential, metav1.GetOptions{})
			if err != nil {
				b.Log.Error(err, "error while getting baremetaldef secret in cluster")
				return err
			}
			data := secret.Object["data"].(map[string]interface{})
			encodedPassword := fmt.Sprintf("%v", data["password"])
			password, _ := base64.StdEncoding.DecodeString(encodedPassword)
			encodedusername := fmt.Sprintf("%v", data["username"])
			username, _ := base64.StdEncoding.DecodeString(encodedusername)

			var serverStored = ServerData{
				Id:          servername,
				BareMetal:   bareMetalDef.Name,
				Schema:      urlSchema,
				BmcUsername: string(username),
				BmcPassword: string(password),
				Url:         bareMetalDef.Spec.Url,
			}
			b.AddServerInPoolStore(poolname, &serverStored)
		}

	}

	return nil
}

func (b *Broker) IsBrokerDataEmpty() bool {
	return len(b.Pools) == 0
}

func (b *Broker) SignChallenge(w http.ResponseWriter, r *http.Request, poolId string) {
	if !b.authentPool(w, r, poolId) {
		return
	}

	b.Log.Info("Sign Challenge", "pool", poolId)
	w.Header().Set("Content-Type", "application/json")
	var postData ChallengeRequest
	var response ChallengeResponse

	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&postData)

	b.Mutex.Lock()
	defer b.Mutex.Unlock()

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		msg := fmt.Sprintf("Error during body request decoding (%s)", err.Error())
		message := ErrorResponseNetdef{Message: msg, Code: OTHER_ERROR}
		json.NewEncoder(w).Encode(&message)
		return
	}

	challengeStr := postData.Challenge

	if challengeStr != "" {
		stringToSign := fmt.Sprintf("%s%s", poolId, challengeStr)

		signedChallenge, err := Sign(stringToSign, b.Log)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			message := ErrorResponseNetdef{Message: "cannot sign challenge", Code: OTHER_ERROR}
			json.NewEncoder(w).Encode(&message)
			return

		}

		response.Challenge = challengeStr
		response.SignedChallenge = &signedChallenge

		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(&response)
		return

	} else {
		w.WriteHeader(http.StatusNotFound)
		message := ErrorResponseNetdef{Message: "empty challenge", Code: OTHER_ERROR}
		json.NewEncoder(w).Encode(&message)
		return
	}
}

func (b *Broker) TestVerifySignature(w http.ResponseWriter, r *http.Request) {
	if !b.authentInfra(w, r) {
		return
	}
	b.VerifySignature(w, r)
}

func (b *Broker) VerifySignature(w http.ResponseWriter, r *http.Request) {
	b.Log.Info("Signature verification")
	w.Header().Set("Content-Type", "application/json")
	var postData SignatureRequest
	var response SignatureResponse

	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&postData)

	b.Mutex.Lock()
	defer b.Mutex.Unlock()

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		msg := fmt.Sprintf("Error during body request decoding (%s)", err.Error())
		message := ErrorResponseNetdef{Message: msg, Code: OTHER_ERROR}
		json.NewEncoder(w).Encode(&message)
		return
	}

	challengeStr := postData.Challenge
	signatureStr := postData.Signature

	if challengeStr != "" && signatureStr != "" {
		err := Verify(challengeStr, signatureStr, b.Log)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			message := ErrorResponseNetdef{Message: "cannot verify challenge", Code: CHECK_CERTIFICATE_ERROR}
			json.NewEncoder(w).Encode(&message)
			return
		}

		response.SignatureVerified = "OK"
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(&response)
		return

	} else {
		w.WriteHeader(http.StatusNotFound)
		message := ErrorResponseNetdef{Message: "empty signature or challenge", Code: OTHER_ERROR}
		json.NewEncoder(w).Encode(&message)
		return
	}
}

func Sign(challenge string, logger logr.Logger) (result string, err error) {
	privateKeyFile, err := os.Open("/mnt/tls.key")
	if err != nil {
		return "", err
	}
	pemfileinfo, _ := privateKeyFile.Stat()
	var size int64 = pemfileinfo.Size()
	pembytes := make([]byte, size)
	buffer := bufio.NewReader(privateKeyFile)
	_, err = buffer.Read(pembytes)
	if err != nil {
		logger.Error(err, "error when reading key")
		return "", err
	}

	data, _ := pem.Decode([]byte(pembytes))
	privateKeyFile.Close()

	privateKey, err := x509.ParsePKCS1PrivateKey(data.Bytes)
	if err != nil {
		logger.Error(err, "error when parsing key")
		return "", err
	}

	rng := rand.Reader
	hashed := sha256.Sum256([]byte(challenge))
	signature, err := rsa.SignPKCS1v15(rng, privateKey, crypto.SHA256, hashed[:])
	if err != nil {
		logger.Error(err, "error when signing")
		return "", err
	}
	result = base64.StdEncoding.EncodeToString(signature)

	return result, err
}

func Verify(challenge string, signature string, logger logr.Logger) (err error) {
	PublicKeyFile, err := os.Open("/mnt/tls.crt")
	if err != nil {
		return err
	}
	pemfileinfo, _ := PublicKeyFile.Stat()
	size := pemfileinfo.Size()
	pembytes := make([]byte, size)
	buffer := bufio.NewReader(PublicKeyFile)
	_, err = buffer.Read(pembytes)
	if err != nil {
		logger.Error(err, "error when reading certificate")
		return err
	}
	data, _ := pem.Decode([]byte(pembytes))
	cert, err := x509.ParseCertificate(data.Bytes)
	if err != nil {
		logger.Error(err, "error when parsing certificate")
		return err
	}
	publicKeyImported := cert.PublicKey.(*rsa.PublicKey)
	PublicKeyFile.Close()

	sig, _ := base64.StdEncoding.DecodeString(signature)
	hashed := sha256.Sum256([]byte(challenge))
	err = rsa.VerifyPKCS1v15(publicKeyImported, crypto.SHA256, hashed[:], sig)
	if err != nil {
		logger.Info("verifying signature failed")
		return err
	}
	return nil
}

func (b *Broker) UpdateUserUsage(userId string, namespace string) error {
	var err error = nil
	ctx := context.Background()

	poolUser := &brokerv1.PoolUser{}

	err = retry.RetryOnConflict(retry.DefaultRetry,
		func() error {
			key := client.ObjectKey{Namespace: namespace, Name: userId}
			err = b.Client.Get(ctx, key, poolUser)
			if err != nil {
				b.Log.Error(err, "Failed to get poolUser", "name", userId)
				return err
			}

			userAnnotations := poolUser.GetAnnotations()
			if userAnnotations == nil {
				userAnnotations = make(map[string]string)
			}

			poollDefList := &brokerv1.PoolDefList{}
			err = b.Client.List(ctx, poollDefList, client.InNamespace(namespace))
			if err != nil {
				return err
			}

			CurrentServers := 0
			limits := InitializeLimits(poolUser.Spec.Limits)
			for _, pool := range poollDefList.Items {
				if pool.Spec.UserName != userId {
					continue
				}

				bmdefList, err := b.getUsedBaremetaldefList(namespace, pool.Name)
				if err != nil {
					return err
				}

				nb := len(bmdefList.Items)
				CurrentServers += nb
				for i, limit := range limits {
					v, ok := pool.Spec.LabelSelector[limit.Label]
					if ok && (limit.Value == "" || v == limit.Value) {
						limits[i].Current += nb
					}
				}
			}

			for _, limit := range limits {
				annotationLimit := ""
				if limit.Value == "" {
					annotationLimit = fmt.Sprintf("kanod.io.limits.%s", limit.Label)
				} else {
					annotationLimit = fmt.Sprintf("kanod.io.limits.%s.%s", limit.Label, limit.Value)
				}
				userAnnotations[annotationLimit] = fmt.Sprintf("%d", limit.Current)
			}
			userAnnotations["kanod.io.nbcurrentserver"] = fmt.Sprintf("%d", CurrentServers)
			poolUser.SetAnnotations(userAnnotations)

			err = b.Client.Update(ctx, poolUser)
			if err != nil {
				return err
			}

			return nil
		})
	if err != nil {
		b.Log.Error(err, "Cannot update annotations on pooluser", "name", poolUser.Name)
		return err
	}
	return nil
}

func (b *Broker) getUsedBaremetaldefList(namespace string, poolName string) (*brokerv1.BareMetalDefList, error) {
	lbls := labels.Set{
		KanodPoolNamelabel: poolName,
	}

	bmdefList := &brokerv1.BareMetalDefList{}
	err := b.Client.List(context.Background(), bmdefList, &client.ListOptions{
		Namespace:     namespace,
		LabelSelector: labels.SelectorFromSet(lbls),
	})
	if err != nil {
		b.Log.Error(err, "Failed to list baremetaldef", "namespace", namespace, "namespace")
		return nil, err
	}
	return bmdefList, nil
}

func (b *Broker) getUnusedBaremetaldefList() (*brokerv1.BareMetalDefList, error) {
	// get unused baremetaldef (not containing the label KanodPoolNamelabel)
	bmdefReq, _ := labels.NewRequirement(
		KanodPoolNamelabel,
		selection.DoesNotExist,
		[]string{})
	selector := labels.NewSelector().Add(*bmdefReq)

	unusedBmdefList := &brokerv1.BareMetalDefList{}
	ctx := context.Background()
	err := b.Client.List(ctx, unusedBmdefList,
		&client.ListOptions{
			Namespace:     b.Namespace,
			LabelSelector: selector},
		client.InNamespace(b.Namespace))

	if err != nil {
		b.Log.Error(err, "Failed to list unused baremetaldef")
		return nil, err
	}

	b.Log.Info(fmt.Sprintf("Found %d unused baremetaldef", len(unusedBmdefList.Items)))
	return unusedBmdefList, nil
}

func (b *Broker) getBaremetaldefInPool(namespace string, poolName string, serverName string) (*brokerv1.BareMetalDef, error) {

	lbls := labels.Set{
		KanodPoolNamelabel:   poolName,
		KanodServerNamelabel: serverName,
	}
	bmdefList := &brokerv1.BareMetalDefList{}
	err := b.Client.List(context.Background(), bmdefList, &client.ListOptions{
		Namespace:     namespace,
		LabelSelector: labels.SelectorFromSet(lbls),
	})
	if err != nil {
		b.Log.Error(err, "Failed to get baremetaldef", "poolname", poolName, "server", serverName)
		return nil, err
	}

	if len(bmdefList.Items) > 1 {
		b.Log.Info("inconsistant number of baremetaldef found", "poolname", poolName, "server", serverName)
		return nil, nil
	}

	if len(bmdefList.Items) == 0 {
		return nil, nil

	}

	b.Log.Info("baremetaldef found in pool", "poolName", poolName, "serverName", serverName)
	return &bmdefList.Items[0], nil
}

func (b *Broker) getPooldefByName(poolname string, namespace string) (*brokerv1.PoolDef, error) {
	poolDef := &brokerv1.PoolDef{}

	key := client.ObjectKey{Namespace: namespace, Name: poolname}
	err := b.Client.Get(context.Background(), key, poolDef)

	if err != nil {
		if k8serrors.IsNotFound(err) {
			b.Log.Info("pooldef not found", "name", poolname)
			return nil, nil
		} else {
			b.Log.Error(err, "Failed to get pooldef", "name", poolname)
			return nil, err
		}
	} else {
		return poolDef, nil
	}
}

func (b *Broker) BookOneServer(pool *brokerv1.PoolDef) (bool, error) {
	poolUser := &brokerv1.PoolUser{}

	pooluserName := pool.Spec.UserName

	key := client.ObjectKey{Namespace: b.Namespace, Name: pooluserName}
	err := b.Client.Get(context.Background(), key, poolUser)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			b.Log.Info("poolUser not found", "name", pooluserName)
			return false, nil
		} else {
			b.Log.Error(err, "Failed to get poolUser", "name", pooluserName)
			return false, err
		}
	}

	userAnnotations := poolUser.GetAnnotations()

	if userAnnotations == nil {
		userAnnotations = make(map[string]string)
	}

	valStr := userAnnotations["kanod.io.nbcurrentserver"]
	nbcurrentserver, _ := strconv.Atoi(valStr)
	if pool.Spec.MaxServers >= 0 && nbcurrentserver+1 > pool.Spec.MaxServers {
		return false, nil
	}

	for _, limit := range poolUser.Spec.Limits {
		v, ok := pool.Spec.LabelSelector[limit.Label]
		if ok && (limit.Value == "" || v == limit.Value) {
			annotationLimit := ""
			if limit.Value == "" {
				annotationLimit = fmt.Sprintf("kanod.io.limits.%s", limit.Label)
			} else {
				annotationLimit = fmt.Sprintf("kanod.io.limits.%s.%s", limit.Label, limit.Value)
			}

			valStr, ok = userAnnotations[annotationLimit]
			if !ok {
				valStr = ""
			}
			val, _ := strconv.Atoi(valStr)

			if val+1 > limit.Max {
				return false, nil
			}
		}
	}

	userAnnotations["kanod.io.nbcurrentserver"] = fmt.Sprintf("%d", (nbcurrentserver + 1))

	for _, limit := range poolUser.Spec.Limits {
		v, ok := pool.Spec.LabelSelector[limit.Label]
		if ok && (limit.Value == "" || v == limit.Value) {
			annotationLimit := ""
			if limit.Value == "" {
				annotationLimit = fmt.Sprintf("kanod.io.limits.%s", limit.Label)
			} else {
				annotationLimit = fmt.Sprintf("kanod.io.limits.%s.%s", limit.Label, limit.Value)
			}

			valStr = userAnnotations[annotationLimit]
			val, _ := strconv.Atoi(valStr)
			userAnnotations[annotationLimit] = fmt.Sprintf("%d", (val + 1))
		}
	}

	poolUser = &brokerv1.PoolUser{}
	key = client.ObjectKey{Namespace: b.Namespace, Name: pooluserName}

	err = retry.RetryOnConflict(retry.DefaultRetry,
		func() error {
			if err := b.Client.Get(context.Background(), key, poolUser); err != nil {
				return err
			}
			poolUser.SetAnnotations(userAnnotations)

			return b.Client.Update(context.Background(), poolUser)
		})

	if err != nil {
		b.Log.Error(err, "unable to update pooluser annotations")
		return false, err
	}

	return true, nil
}

func (b *Broker) ReleaseOneServer(pool *brokerv1.PoolDef) error {
	poolUser := &brokerv1.PoolUser{}

	pooluserName := pool.Spec.UserName

	key := client.ObjectKey{Namespace: b.Namespace, Name: pooluserName}
	err := b.Client.Get(context.Background(), key, poolUser)
	if err != nil {
		return err
	}

	userAnnotations := poolUser.GetAnnotations()

	if userAnnotations == nil {
		b.Log.Error(err, "missing pooluser annotations for limits")
		return err
	}

	valStr := userAnnotations["kanod.io.nbcurrentserver"]
	nbcurrentserver, _ := strconv.Atoi(valStr)

	userAnnotations["kanod.io.nbcurrentserver"] = fmt.Sprintf("%d", (nbcurrentserver - 1))

	for _, limit := range poolUser.Spec.Limits {
		v, ok := pool.Spec.LabelSelector[limit.Label]
		if ok && (limit.Value == "" || v == limit.Value) {
			annotationLimit := ""
			if limit.Value == "" {
				annotationLimit = fmt.Sprintf("kanod.io.limits.%s", limit.Label)
			} else {
				annotationLimit = fmt.Sprintf("kanod.io.limits.%s.%s", limit.Label, limit.Value)
			}

			valStr = userAnnotations[annotationLimit]
			val, _ := strconv.Atoi(valStr)
			userAnnotations[annotationLimit] = fmt.Sprintf("%d", (val - 1))
		}
	}

	poolUser = &brokerv1.PoolUser{}
	key = client.ObjectKey{Namespace: b.Namespace, Name: pooluserName}

	err = retry.RetryOnConflict(retry.DefaultRetry,
		func() error {
			if err := b.Client.Get(context.Background(), key, poolUser); err != nil {
				return err
			}
			poolUser.SetAnnotations(userAnnotations)

			return b.Client.Update(context.Background(), poolUser)
		})

	if err != nil {
		b.Log.Error(err, "unable to update pooluser annotations")
		return err
	}

	return nil
}

func (b *Broker) GetSecret(secretName string) (*corev1.Secret, error) {

	key := client.ObjectKey{Namespace: b.Namespace, Name: secretName}
	secret := corev1.Secret{}
	err := b.Client.Get(context.Background(), key, &secret)

	if err != nil {
		if k8serrors.IsNotFound(err) {
			b.Log.Info("pooluser secret not found", "name", secretName)
			return nil, nil
		} else {
			b.Log.Error(err, "cannot get secret for pooluser", "user", secretName)
			return nil, err
		}
	}

	return &secret, nil
}

func (b *Broker) GetPoolUser(poolUserName string) (*brokerv1.PoolUser, error) {
	poolUser := &brokerv1.PoolUser{}
	key := client.ObjectKey{Namespace: b.Namespace, Name: poolUserName}
	err := b.Client.Get(context.Background(), key, poolUser)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			b.Log.Info("pooluser  not found", "name", poolUserName)
			return nil, nil
		} else {
			b.Log.Error(err, "cannot get pooluser", "user", poolUserName)
			return nil, err
		}
	}
	return poolUser, nil
}
