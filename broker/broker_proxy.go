package broker

import (
	"fmt"
	"net/url"
	"os"

	"github.com/gorilla/mux"
	brokerv1 "gitlab.com/Orange-OpenSource/kanod/brokerdef/api/v1"
)

// BrokerProxy is the type of the implementation of a Redfish broker using
// a proxy to virtualize the BMC of servers.
type BrokerProxy struct {
	RedfishDomain string
	Broker        *Broker
}

func (bp *BrokerProxy) Init(b *Broker, r *mux.Router) {
	bp.Broker = b
	redfishDomain := os.Getenv("REDFISH_DOMAIN")
	if redfishDomain == "" {
		redfishDomain = "kanod.io"
	}
	bp.RedfishDomain = redfishDomain
	r.HandleFunc("/redfish/{rest:.*}", b.processRedfishRequest)
}

func (bp *BrokerProxy) ServerResponse(bareMetalDef *brokerv1.BareMetalDef) *ServerResponse {
	annotations := bareMetalDef.GetAnnotations()
	poolId := annotations[KanodPoolnameAnnotation]
	serverId := annotations[KanodServernameAnnotation]
	schema := annotations[KanodRedfishSchemaAnnotation]

	parsedUrl, _ := url.Parse(bareMetalDef.Spec.Url)
	serverName := fmt.Sprintf("%s-%s", poolId, serverId)
	url := fmt.Sprintf("%s:%s.%s%s", schema, serverName, bp.RedfishDomain, parsedUrl.Path)
	response := ServerResponse{
		Id:                             serverId,
		BareMetal:                      &bareMetalDef.Name,
		Url:                            &url,
		K8sLabels:                      &bareMetalDef.Spec.K8sLabels,
		K8sAnnotations:                 &bareMetalDef.Spec.K8sAnnotations,
		MacAddress:                     &bareMetalDef.Spec.MacAddress,
		RootDeviceHints:                bareMetalDef.Spec.RootDeviceHints,
		DisableCertificateVerification: &bareMetalDef.Spec.DisableCertificateVerification,
	}
	return &response

}

func (bp *BrokerProxy) BookServer(poolUserName string, poolUserSecret string, server *brokerv1.BareMetalDef) error {
	return nil
}

func (bp *BrokerProxy) ReleaseServer(poolUserName string, server *brokerv1.BareMetalDef) error {
	return nil
}
