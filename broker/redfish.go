/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package broker

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"

	"github.com/go-logr/logr"
)

type AuthRequest struct {
	UserName string `json:"username"`
	Password string `json:"password"`
}

// ErrorProxyResponse is the error message with redfish formmat
type ErrorProxyResponse struct {
	Error ErrorRedfish `json:"error"`
}

type ErrorRedfish struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

func BrokerProxyError(w http.ResponseWriter, logger logr.Logger, err error, message string, statusCode int) {
	w.WriteHeader(statusCode)
	logger.Info("Broker proxy - Sending back error", "message", message)
	content := ErrorProxyResponse{Error: ErrorRedfish{Message: message, Code: "Base.1.0.GeneralError"}}
	if err := json.NewEncoder(w).Encode(&content); err != nil {
		logger.Error(err, "json encoding error during error reporting")
	}
}

func (b *Broker) processRedfishRequest(w http.ResponseWriter, originRequest *http.Request) {
	var newRequest *http.Request
	var stringBody interface{}

	transCfg := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: transCfg}
	username, password, isBasicAuth := originRequest.BasicAuth()

	hostOrigin := originRequest.Host
	pos := strings.Index(hostOrigin, ".")
	if pos != -1 {
		hostOrigin = hostOrigin[0:pos]
	}
	pos = strings.Index(hostOrigin, ":")
	if pos != -1 {
		hostOrigin = hostOrigin[0:pos]
	}
	split := strings.Split(hostOrigin, "-")
	if len(split) != 2 {
		BrokerProxyError(w, b.Log, errors.New("invalid pool or server"), "cannot connect to the server, invalid pool or server", http.StatusNotFound)
		return
	}
	poolRequested := split[0]
	serverRequested := split[1]

	if isBasicAuth {
		authOk := b.VerifyPoolAuthent(username, password, poolRequested)
		if !authOk {
			BrokerProxyError(w, b.Log, errors.New("bad auth for connection"), "Unauthorized - invalid credentials", http.StatusUnauthorized)
			return
		}
	}

	redfishAddr := b.getRedfishAddr(poolRequested, serverRequested)

	if redfishAddr == "" {
		BrokerProxyError(w, b.Log, errors.New("server not found"), "invalid pool or server", http.StatusNotFound)
		return
	}

	parsedUrl, _ := url.Parse(originRequest.URL.Path)
	redfishPath := parsedUrl.Path

	if strings.HasPrefix(redfishPath, "/redfish/v1/AccountService") {
		msg := fmt.Sprintf("Connection attempt on account service for pool %s with username %s", poolRequested, username)
		BrokerProxyError(w, b.Log, errors.New(msg), "redfish proxy : access forbidden", http.StatusForbidden)
		return
	}

	redfishUsername, redfishPassword := b.getCredRedfish(poolRequested, serverRequested)

	if (redfishPath == "/redfish/v1/SessionService/Sessions") && (originRequest.Method == "POST") {
		var postData AuthRequest

		decoder := json.NewDecoder(originRequest.Body)
		err := decoder.Decode(&postData)

		if err != nil {
			BrokerProxyError(w, b.Log, err, "Error during post data decoding", http.StatusBadRequest)
			return
		}
		postBody, _ := json.Marshal(map[string]string{"UserName": redfishUsername, "Password": redfishPassword})

		newRequest, err = http.NewRequest(originRequest.Method, redfishAddr+redfishPath, bytes.NewBuffer(postBody))
		if err != nil {
			BrokerProxyError(w, b.Log, err, "redfish proxy : cannot create new request", http.StatusInternalServerError)
			return
		}
		copyHeader(originRequest.Header, newRequest.Header)

		newRequest.Header.Set("Content-Type", "application/json; charset=UTF-8")

	} else {
		var err error
		newRequest, err = http.NewRequest(originRequest.Method, redfishAddr+redfishPath, nil)
		b.Log.Info("DEBUG", "originRequest.Method", originRequest.Method, "redfishAddr+redfishPath", redfishAddr+redfishPath)
		if err != nil {
			BrokerProxyError(w, b.Log, err, "redfish proxy : cannot create new request", http.StatusInternalServerError)
			return
		}

		copyHeader(originRequest.Header, newRequest.Header)

		if originRequest.ContentLength != 0 {
			newRequest.Body = originRequest.Body
		}
		if isBasicAuth {
			newRequest.SetBasicAuth(redfishUsername, redfishPassword)
		}

		val := originRequest.Header.Get("x-auth-token")
		if val != "" {
			newRequest.Header.Set("x-auth-token", val)
		}
		newRequest.Header.Set("Content-Type", "application/json; charset=UTF-8")

	}

	if newRequest.Body != nil {
		bodyBuffer, _ := io.ReadAll(newRequest.Body)
		b.Log.Info("DEBUG", "bodyBuffer", bodyBuffer)

		json.Unmarshal([]byte(bodyBuffer), &stringBody)
		reader := io.NopCloser(bytes.NewBuffer(bodyBuffer))
		newRequest.Body = reader
	} else {
		stringBody = ""
	}
	b.Log.Info("DEBUG - Sent request : ", "Method", newRequest.Method, "URL", newRequest.URL, " header: ", newRequest.Header, " body: ", stringBody)

	resp, err := client.Do(newRequest)
	if err != nil {
		BrokerProxyError(w, b.Log, err, "redfish proxy : error during new request sending", http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		BrokerProxyError(w, b.Log, err, "redfish proxy : error during body response decoding", http.StatusInternalServerError)
		return
	}

	copyHeader(resp.Header, w.Header())

	// process redirection when used with Virtual Redfish BMC
	if resp.StatusCode == http.StatusTemporaryRedirect {
		val, _ := resp.Location()
		newUrl := val.Scheme + "://" + originRequest.Host + val.Path
		w.Header().Set("Location", newUrl)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(resp.StatusCode)
	w.Write(body)
}

func copyHeader(src http.Header, dest http.Header) {
	for key, value := range src {
		dest.Set(key, strings.Join(value, ","))
	}
}
