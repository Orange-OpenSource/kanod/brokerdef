Overview
========
The brokerdef operator acts as a broker between the real baremetal resources
and the management cluster (through BareMetalPool resource).

The brokerdef has 3 mains functions:

* storing the definition of the baremetals, the pools and the poolusers
* exposing an API to assign a baremetal to a pool (server creation)
* relaying redfish requests between the management cluster and the baremetals;
  It is done :

  - either through a proxy
  - or by using accounts on the BMC of the baremetal


BareMetals and pools creation
=============================
The creation of baremetal/pool/pooluser resources are realized with
the corresponding custom Resources ``BareMetalDef``,  ``PoolDef`` and ``Pooluser``:

* ``BareMetalDef`` contains the description of a baremetal server with all the information
  needed to access it (address, credentials). It also contains the information to generate
  a barematalhost custom resource.
* ``PoolDef`` contains the description of a pool associated with one user
  and eventually a list of labels to select the baremetals for this pool
* ``Pooluser`` contains the description of a user with the credentials associated

Server creation API
===================
From the broker point of view, a server is a link between a baremetal and a pool.

The broker offers a REST API with three levels:

* **pool admin** : for the creation/list/deletion of a server
  The pool is selected with the credentials passed in the basicauth field of the request header

  * POST `/pools`

    Creation of a pool

    .. code-block:: yaml

        type: object
        description: a pool definition
        properties:
          id:
            type: string
            description: id to identify the pool
          LabelSelector:
            additionalProperties:
              type: string
            type: object
            description: labels associated to the pool
          MaxServers:
            type: int
            description: max servers for the pool

  * POST `/pools/${pool-id}/servers/`

    Creation of a server in the pool ${pool-id} with the following payload schema:

    .. code-block:: yaml

        type: object
        description: a proxified server
        properties:
          id:
            type: string
            description: id to identify the server
          schema:
            type: string
            description: schema used for the BMC address in the baremetalhost

  * GET `/pools/${pool-id}/servers/`

    Retrieve informations for all the servers of the pool ${pool-id}

  * GET `/pools/${pool-id}/servers/${id}`

    Retrieve informations for the server ${id} of the pool ${pool-id}

  * DELETE `/pools/${pool-id}/servers/${id}`

    Delete the server ${id} in the pool ${pool-id}

  * POST `/signchallenge`

    Creation of a server in the pool ${pool-id} with the following payload schema:

    .. code-block:: yaml

        type: object
        description: a proxified server
        properties:
          challenge:
            type: string
            description: string to sign


* **broker admin** :

  * POST `/verifysignature`

    Verify a signature, with the following payload schema:

    .. code-block:: yaml

        type: object
        description: signature verification
        properties:
          challenge:
            type: string
            description: initial challenge
          signature:
            type: string
            description: signed challenge

  * GET `/poolusers/{pooluserid}/getpassword`

    Get the password associated to the pooluser *pooluserid*


* **redfish BMC** : acting as a virtual BMC, the payload is transmitted to a real BMC
  according to the translation tables of the pool
