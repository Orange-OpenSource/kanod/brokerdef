BareMetalDef API
================

Specification
-------------
.. jsonschema:: baremetaldefs-schema.yaml#/spec/versions/0/schema/openAPIV3Schema/properties/spec
    :lift_title:
    :lift_description:
    :lift_definitions:
    :auto_reference:
    :auto_target:

Status
------
.. jsonschema:: baremetaldefs-schema.yaml#/spec/versions/0/schema/openAPIV3Schema/properties/status
    :lift_title:
    :lift_description:
    :lift_definitions:
    :auto_reference:
    :auto_target:

PoolDef API
===========

Specification
-------------
.. jsonschema:: pooldefs-schema.yaml#/spec/versions/0/schema/openAPIV3Schema/properties/spec
    :lift_title:
    :lift_description:
    :lift_definitions:
    :auto_reference:
    :auto_target:

Status
------
.. jsonschema:: pooldefs-schema.yaml#/spec/versions/0/schema/openAPIV3Schema/properties/status
    :lift_title:
    :lift_description:
    :lift_definitions:
    :auto_reference:
    :auto_target:

PoolUser API
============

Specification
-------------
.. jsonschema:: poolusers-schema.yaml#/spec/versions/0/schema/openAPIV3Schema/properties/spec
    :lift_title:
    :lift_description:
    :lift_definitions:
    :auto_reference:
    :auto_target:

Status
------
.. jsonschema:: poolusers-schema.yaml#/spec/versions/0/schema/openAPIV3Schema/properties/status
    :lift_title:
    :lift_description:
    :lift_definitions:
    :auto_reference:
    :auto_target:
