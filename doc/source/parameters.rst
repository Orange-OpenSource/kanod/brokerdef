Brokerdef parameters
====================
The credentials for the ``broker admin`` user of the Redfish Broker 
are defined in the secret ``cred-admin``
stored in the ``brokerdef-system`` namespace.
 
