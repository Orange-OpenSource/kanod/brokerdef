/*
Copyright 2020 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// PoolUserSpec defines the desired state of PoolUser
type PoolUserSpec struct {
	// CredentialName is the name of the secret containing the password associated
	// with the pool user
	CredentialName string `json:"credentialName"`
	// MaxServers it the maximum number of servers that can be registered for this user
	// +kubebuilder:default=-1
	MaxServers int `json:"maxServers,omitempty"`
	// Limits imposed on the resources attached to this user.
	Limits []PoolLimit `json:"limits,omitempty"`
}

type PoolLimit struct {
	// Label is the label on the baremetal host that is checked
	Label string `json:"label"`
	// The machine is counted if the label has this value. If not specified,
	// it is the presence of the label that is counted.
	Value string `json:"value,omitempty"`
	// Max is the maximum number of machines with this configuration for this
	// user.
	Max int `json:"max"`
}

type PoolUsage struct {
	// Label is the label on the baremetal host that is checked
	Label string `json:"label"`
	// The machine is counted if the label has this value. If not specified
	// It is the presence of the label that is counted.
	Value string `json:"value,omitempty"`
	// Current is the current number of machines with this configuration used
	// by this user.
	Current int `json:"current"`
}

// PoolUserStatus defines the observed state of PoolUser
type PoolUserStatus struct {
	Booked int         `json:"used"`
	Usage  []PoolUsage `json:"usage"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// PoolUser is the Schema for the poolusers API
type PoolUser struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   PoolUserSpec   `json:"spec,omitempty"`
	Status PoolUserStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// PoolUserList contains a list of PoolUser
type PoolUserList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []PoolUser `json:"items"`
}

func init() {
	SchemeBuilder.Register(&PoolUser{}, &PoolUserList{})
}
