/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	bmh "github.com/metal3-io/baremetal-operator/apis/metal3.io/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// BareMetalDefSpec defines the desired state of BareMetalDef
type BareMetalDefSpec struct {
	// Id of the baremetal
	Id string `json:"id"`
	// Name of pooldef booking the baremetal or empty
	PoolDef string `json:"poolDef,omitempty"`
	// List of network bindings
	NetDefMap map[string]string `json:"netDefMap,omitempty"`
	// BMC address of the baremetal
	Url string `json:"url"`
	// Name of the secret storing the BMC credentials of the baremetal
	BareMetalCredential string `json:"baremetalcredential"`
	// MAC address of the baremetal
	MacAddress string `json:"macAddress"`
	// K8S labels which will be assigned to the baremetalhost created for this baremetal
	K8sLabels map[string]string `json:"k8slabels,omitempty"`
	// K8S annotations which will be assigned to the baremetalhost created for this baremetal
	K8sAnnotations map[string]string `json:"k8sannotations,omitempty"`
	// DeviceName value which will be assigned to the baremetalhost created for this baremetal
	// (for example : ``sda`` for real baremetal, ``vda`` for libvirt VM)
	RootDeviceHints *bmh.RootDeviceHints `json:"rootDeviceHints,omitempty"`
	// DisableCertificateVerification value which will be assigned to the baremetalhost created for this baremetal
	DisableCertificateVerification bool `json:"disablecertificateverification"`
}

// BareMetalDefStatus defines the observed state of BareMetalDef
type BareMetalDefStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Status of the BareMetalDef
	State string `json:"state"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:printcolumn:name="Id",type=string,JSONPath=`.spec.id`
// +kubebuilder:printcolumn:name="Status",type="string",JSONPath=".status.state"
type BareMetalDef struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   BareMetalDefSpec   `json:"spec,omitempty"`
	Status BareMetalDefStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// BareMetalDefList contains a list of BareMetalDef
type BareMetalDefList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []BareMetalDef `json:"items"`
}

func init() {
	SchemeBuilder.Register(&BareMetalDef{}, &BareMetalDefList{})
}
