/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// PoolDefSpec defines the desired state of PoolDef
type PoolDefSpec struct {
	// Name of the user associated with the pool
	UserName string `json:"username"`
	// label for the selection of the baremetal which will be associated with the pool
	LabelSelector map[string]string `json:"labelSelector,omitempty"`
	// Number max of baremetal which can be associated with the pool a negative value
	// means no limit. 0 disable the pool.
	// +kubebuilder:default=-1
	MaxServers int `json:"maxServers,omitempty"`
	// NetworkDefinition list
	NetDefMap map[string]string `json:"netDefMap,omitempty"`
}

// PoolDefStatus defines the observed state of PoolDef
type PoolDefStatus struct {
	// Status of the PoolDef
	State string `json:"state"`
	// +optional
	NetworkDefinitions []string `json:"networkdefinitions,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:printcolumn:name="Status",type="string",JSONPath=".status.state"
// +kubebuilder:printcolumn:name="NetworkDefinitions",type="string",JSONPath=".status.networkdefinitions"
type PoolDef struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   PoolDefSpec   `json:"spec,omitempty"`
	Status PoolDefStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// PoolDefList contains a list of PoolDef
type PoolDefList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []PoolDef `json:"items"`
}

func init() {
	SchemeBuilder.Register(&PoolDef{}, &PoolDefList{})
}
