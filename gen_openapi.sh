#!/bin/bash

# Openapi broker rest api
# specification edition: # https://editor.swagger.io/
# code generation: https://github.com/deepmap/oapi-codegen

go install github.com/deepmap/oapi-codegen/cmd/oapi-codegen@latest

oapi-codegen --generate gorilla,types,spec,client  -package broker broker/broker_api.yaml > broker/broker_api.go
sed -i  's!import (!& \n	bmh \"github.com/metal3-io/baremetal-operator/apis/metal3.io/v1alpha1\"!' broker/broker_api.go
sed -i  's!RootDeviceHints \*map\[string\]interface{}!RootDeviceHints *bmh.RootDeviceHints!' broker/broker_api.go

sed -i  's!const (!type AuthScope string\n&!' broker/broker_api.go
sed -i  's!PoolBasicAuthScopes =!PoolBasicAuthScopes AuthScope =!' broker/broker_api.go
