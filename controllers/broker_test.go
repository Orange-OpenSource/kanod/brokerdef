package controllers

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"testing"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"golang.org/x/crypto/bcrypt"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/envtest"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	brokerv1 "gitlab.com/Orange-OpenSource/kanod/brokerdef/api/v1"
	broker "gitlab.com/Orange-OpenSource/kanod/brokerdef/broker"
	corev1 "k8s.io/api/core/v1"
	//+kubebuilder:scaffold:imports
)

const (
	REDFISH_PORT = 10000
	BROKER_PORT  = 9000
	PASSWORD     = "secret"
)

var cfg *rest.Config
var k8sClient client.Client
var testEnv *envtest.Environment
var brokerImpl *broker.Broker

func TestAPIs(t *testing.T) {
	RegisterFailHandler(Fail)

	RunSpecs(t, "Broker Tests")
}

func createCredential(ctx context.Context, name string, user string) {
	credential := &corev1.Secret{
		TypeMeta: metav1.TypeMeta{
			APIVersion: "v1",
			Kind:       "Secret",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: "default",
		},
		Data: map[string][]byte{
			"username": []byte(user),
			"password": []byte(fmt.Sprintf("pwd-%s", name)),
		},
	}
	Expect(k8sClient.Create(ctx, credential)).Should(
		Succeed(),
		"Credential creation must succeed (secret %s)", name)
}

func createBareMetalDef(ctx context.Context, name string, labels map[string]string) {
	createCredential(ctx, name, "redfish")
	bmd := &brokerv1.BareMetalDef{
		TypeMeta: metav1.TypeMeta{
			APIVersion: "broker.kanod.io/v1",
			Kind:       "BareMetalDef",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: "default",
			Labels:    labels,
		},
		Spec: brokerv1.BareMetalDefSpec{
			Id:                  name,
			Url:                 fmt.Sprintf("https://localhost:%d", REDFISH_PORT),
			BareMetalCredential: name,
		},
	}
	Expect(k8sClient.Create(ctx, bmd)).Should(
		Succeed(),
		"BaremetalDef %s creation should succeed", name)
}

func createUser(ctx context.Context, name string) {
	createCredential(ctx, name, name)
	user := &brokerv1.PoolUser{
		TypeMeta: metav1.TypeMeta{
			APIVersion: "broker.kanod.io/v1",
			Kind:       "PoolUser",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: "default",
		},
		Spec: brokerv1.PoolUserSpec{
			CredentialName: name,
			MaxServers:     4,
			Limits: []brokerv1.PoolLimit{
				{Label: "c1", Value: "v", Max: 1},
				{Label: "c1", Value: "w", Max: 1},
				{Label: "c2", Value: "b", Max: 2},
			},
		},
	}
	Expect(k8sClient.Create(ctx, user)).Should(
		Succeed(),
		"User %s creation should succeed", name)
}

func deleteCredential(ctx context.Context, name string) {
	key := client.ObjectKey{Name: name, Namespace: "default"}
	sec := &corev1.Secret{}
	Expect(k8sClient.Get(ctx, key, sec)).Should(
		Succeed(),
		"Acces credential secret %s before deletion", name)
	Expect(k8sClient.Delete(ctx, sec)).Should(
		Succeed(),
		"Credential secret %s deletion should succeed", name)

}

func deleteBareMetalDef(ctx context.Context, name string) {
	deleteCredential(ctx, name)
	key := client.ObjectKey{Name: name, Namespace: "default"}
	bmd := &brokerv1.BareMetalDef{}
	Expect(k8sClient.Get(ctx, key, bmd)).Should(
		Succeed(),
		"Acces BareMealDef %s before deletion", name)
	Expect(k8sClient.Delete(ctx, bmd)).Should(
		Succeed(),
		"BaremetalDef %s deletion should succeed", name)
}

func deleteUser(ctx context.Context, name string) {
	deleteCredential(ctx, name)
	key := client.ObjectKey{Name: name, Namespace: "default"}
	user := &brokerv1.PoolUser{}
	Expect(k8sClient.Get(ctx, key, user)).Should(
		Succeed(),
		"Acces PoolUser %s before deletion", name)
	Expect(k8sClient.Delete(ctx, user)).Should(
		Succeed(),
		"PoolUser %s deletion should succeed", name)
}

var (
	Bm_u  = map[string]string{"c1": "u", "c2": "a"}
	Bm_v  = map[string]string{"c1": "v", "c2": "b"}
	Bm_w  = map[string]string{"c1": "w", "c2": "c"}
	Bm_x  = map[string]string{"c1": "x"}
	Req_u = map[string]string{"c1": "u"}
	Req_v = map[string]string{"c1": "v"}
	Req_w = map[string]string{"c1": "w"}
	Req_x = map[string]string{"c1": "w"}
	Req_b = map[string]string{"c2": "b"}
)

var _ = BeforeSuite(func() {
	logf.SetLogger(zap.New(zap.WriteTo(GinkgoWriter), zap.UseDevMode(true)))

	By("bootstrapping test environment")
	testEnv = &envtest.Environment{
		CRDDirectoryPaths:     []string{filepath.Join("..", "config", "crd", "bases")},
		ErrorIfCRDPathMissing: true,
	}

	var err error
	cfg, err = testEnv.Start()
	Expect(err).NotTo(HaveOccurred(), "Starting testenv")
	Expect(cfg).NotTo(BeNil(), "testenv config not empty")

	err = brokerv1.AddToScheme(scheme.Scheme)
	Expect(err).NotTo(HaveOccurred(), "Scheme handling")

	//+kubebuilder:scaffold:scheme

	k8sClient, err = client.New(cfg, client.Options{Scheme: scheme.Scheme})
	Expect(err).NotTo(HaveOccurred(), "K8s client creation")
	Expect(k8sClient).NotTo(BeNil(), "K8s client exists")

	hash, err := bcrypt.GenerateFromPassword([]byte(PASSWORD), bcrypt.DefaultCost)
	Expect(err).ToNot(HaveOccurred(), "Generate bcrypt password")
	os.Setenv("BROKER_NAMESPACE", "default")
	os.Setenv("ADMIN_USERNAME", "admin")
	os.Setenv("ADMIN_PASSWORD", string(hash))
	os.Setenv("BROKER_ADDRESS", fmt.Sprintf(":%d", BROKER_PORT))

	k8sManager, err := ctrl.NewManager(cfg, ctrl.Options{
		Scheme:    scheme.Scheme,
		Namespace: "default",
	})
	Expect(err).ToNot(HaveOccurred(), "Controller Manager creation")

	brokerImpl = broker.NewBroker(
		k8sManager.GetClient(), k8sManager.GetLogger(),
		k8sManager.GetScheme(), &broker.BrokerProxy{})

	err = brokerImpl.InitBrokerData(cfg, k8sManager.GetLogger())
	Expect(err).ToNot(HaveOccurred(), "InitBrokerData")
	// Launch broker server in background
	err = (&PoolDefReconciler{
		Client: k8sManager.GetClient(),
		Log:    ctrl.Log.WithName("controllers").WithName("PoolDef"),
		Scheme: k8sManager.GetScheme(),
		Broker: brokerImpl,
	}).SetupWithManager(k8sManager)
	Expect(err).ToNot(HaveOccurred(), "PoolDef controller creation")

	err = (&BareMetalDefReconciler{
		Client: k8sManager.GetClient(),
		Log:    ctrl.Log.WithName("controllers").WithName("BareMetalDef"),
		Scheme: k8sManager.GetScheme(),
		Broker: brokerImpl,
	}).SetupWithManager(k8sManager)
	Expect(err).ToNot(HaveOccurred(), "BareMetalDef controller creation")

	err = (&PoolUserReconciler{
		Client: k8sManager.GetClient(),
		Log:    ctrl.Log.WithName("controllers").WithName("PoolUser"),
		Scheme: k8sManager.GetScheme(),
		Broker: brokerImpl,
	}).SetupWithManager(k8sManager)
	Expect(err).ToNot(HaveOccurred(), "PoolUser controller creation")

	go func() {
		brokerImpl.HandleBrokerApiRequests()
	}()

	go func() {
		defer GinkgoRecover()
		err = k8sManager.Start(context.Background())
		Expect(err).ToNot(HaveOccurred(), "failed to run manager")
	}()

}, 60)

var _ = AfterSuite(func() {
	By("tearing down the test environment")
	err := testEnv.Stop()
	Expect(err).NotTo(HaveOccurred(), "Tearing down suite failure")
})

var (
	nbUsers = 4
	nbBm_u  = 4
	nbBm_v  = 8
	nbBm_w  = 6
)

var _ = BeforeEach(func() {
	ctx := context.Background()
	for i := 1; i < nbBm_u; i++ {
		createBareMetalDef(ctx, fmt.Sprintf("bmu-%d", i), Bm_u)
	}
	for i := 1; i < nbBm_v; i++ {
		createBareMetalDef(ctx, fmt.Sprintf("bmv-%d", i), Bm_v)
	}
	for i := 1; i < nbBm_w; i++ {
		createBareMetalDef(ctx, fmt.Sprintf("bmw-%d", i), Bm_w)
	}
	for i := 1; i <= nbUsers; i++ {
		createUser(ctx, fmt.Sprintf("user-%d", i))
	}

})

var _ = AfterEach(func() {
	ctx := context.Background()
	for i := 1; i < nbBm_u; i++ {
		deleteBareMetalDef(ctx, fmt.Sprintf("bmu-%d", i))
	}
	for i := 1; i < nbBm_v; i++ {
		deleteBareMetalDef(ctx, fmt.Sprintf("bmv-%d", i))
	}
	for i := 1; i < nbBm_w; i++ {
		deleteBareMetalDef(ctx, fmt.Sprintf("bmw-%d", i))
	}
	for i := 1; i <= nbUsers; i++ {
		deleteUser(ctx, fmt.Sprintf("user-%d", i))
	}

})

func addBasicAuth(username string, password string) broker.RequestEditorFn {
	return func(ctx context.Context, req *http.Request) error {
		req.SetBasicAuth(string(username), string(password))
		return nil
	}
}
func createPool(client *broker.ClientWithResponses, user string, pool string, labelSelector map[string]string, status int) {
	size := 3
	request := broker.PoolRequest{
		MaxServers:    &size,
		Id:            &pool,
		LabelSelector: &labelSelector,
	}
	response, err := client.CreatePool(context.Background(),
		request,
		addBasicAuth(user, fmt.Sprintf("pwd-%s", user)))

	Expect(err).ToNot(HaveOccurred(), "Request for creating pool %s failed", pool)
	defer response.Body.Close()
	Expect(response.StatusCode).To(Equal(status), "Status for creating pool %s status", pool)
	bodyBytes, _ := io.ReadAll(response.Body)
	if status == http.StatusCreated {
		Expect(string(bodyBytes)).To(Equal(""))
	} else {
		var jsonBody map[string]interface{}
		json.Unmarshal(bodyBytes, &jsonBody)
		Expect(jsonBody["message"]).NotTo(Equal(""), "error message must not be nil (pool %s creation)", pool)
	}
}

func checkPool(client *broker.ClientWithResponses, user string, pool string) {
	response, err := client.GetPool(context.Background(),
		pool,
		addBasicAuth(user, fmt.Sprintf("pwd-%s", user)))

	Expect(err).ToNot(HaveOccurred(), "Request for pool %s not successful", pool)
	Expect(response.StatusCode).To(Equal(http.StatusOK), "Status of request for pool %s", pool)
	defer response.Body.Close()
	Expect(response.StatusCode).To(Equal(200))
	bodyBytes, _ := io.ReadAll(response.Body)
	var jsonBody map[string]interface{}
	json.Unmarshal(bodyBytes, &jsonBody)
	Expect(jsonBody["username"].(string)).To(Equal(user), "User of pool %s is not right", pool)
	Expect(int(jsonBody["maxServers"].(float64))).To(Equal(3), "Max servers of pool %s is not right", pool)
}

func Count[E any](s []E, f func(E) bool) (c int) {
	for i := range s {
		if f(s[i]) {
			c++
		}
	}
	return
}

func bookServer(client *broker.ClientWithResponses, user string, pool string, server string, statusCode int) {
	body := broker.CreateServerJSONRequestBody{
		Id:     server,
		Schema: "redfish",
	}
	response, err := client.CreateServer(context.Background(), pool, body, addBasicAuth(user, fmt.Sprintf("pwd-%s", user)))
	Expect(err).ToNot(HaveOccurred(), "Server %s in %s for user %s should go through", server, pool, user)
	Expect(response.StatusCode).To(Equal(statusCode), "Server %s creation request in %s for user %s status", server, pool, user)
}

func deleteServer(client *broker.ClientWithResponses, user string, pool string, server string, statusCode int) {
	response, err := client.RemoveServer(context.Background(), pool, server, addBasicAuth(user, fmt.Sprintf("pwd-%s", user)))
	Expect(err).ToNot(HaveOccurred(), "Server %s deletion in %s for user %s should go through", server, pool, user)
	Expect(response.StatusCode).To(Equal(statusCode), "Server %s deletion request in %s for user %s status", server, pool, user)
}

func deletePool(client *broker.ClientWithResponses, user string, pool string, statusCode int) {
	response, err := client.RemovePool(context.Background(), pool, addBasicAuth(user, fmt.Sprintf("pwd-%s", user)))
	Expect(err).ToNot(HaveOccurred(), "Pool %s deletion for user %s should go through", pool, user)
	Expect(response.StatusCode).To(Equal(statusCode), "Pool %s deletion request for user %s status", pool, user)
}

func getServer(client *broker.ClientWithResponses, user string, pool string, server string, statusCode int) {
	response, err := client.GetServerByID(context.Background(), pool, server, addBasicAuth(user, fmt.Sprintf("pwd-%s", user)))
	Expect(err).ToNot(HaveOccurred(), "Server %s get in %s for user %s should go through", server, pool, user)
	Expect(response.StatusCode).To(Equal(statusCode), "Server %s get request in %s for user %s status", server, pool, user)
	bodyText, err := io.ReadAll(response.Body)
	Expect(err).ToNot(HaveOccurred(), "decode servers list response for %s : %s", pool, bodyText)
}

func getAllServer(client *broker.ClientWithResponses, user string, pool string, statusCode int) {
	response, err := client.GetAllServers(context.Background(), pool, addBasicAuth(user, fmt.Sprintf("pwd-%s", user)))
	Expect(err).ToNot(HaveOccurred(), "All servers get in %s for user %s should go through", pool, user)
	Expect(response.StatusCode).To(Equal(statusCode), "All servers get request in %s for user %s status", pool, user)
	bodyText, err := io.ReadAll(response.Body)
	Expect(err).ToNot(HaveOccurred(), "decode servers list response for %s : %s", pool, bodyText)
}

func getPoolUserPassword(client *broker.ClientWithResponses, pooluser string, statusCode int) {
	response, err := client.GetPooluserPassword(context.Background(), pooluser, addBasicAuth("admin", PASSWORD))
	Expect(err).ToNot(HaveOccurred(), "Request password for user %s should go through", pooluser)
	Expect(response.StatusCode).To(Equal(statusCode), "Request password for user %s status", pooluser)
}

func signChallenge(client *broker.ClientWithResponses, user string, pool string, challenge string, statusCode int) {
	body := broker.ChallengeRequest{
		Challenge: challenge,
	}
	response, err := client.SignChallenge(context.Background(), pool, body, addBasicAuth(user, fmt.Sprintf("pwd-%s", user)))
	Expect(err).ToNot(HaveOccurred(), "Sign challenge for user %s should go through", pool)
	Expect(response.StatusCode).To(Equal(statusCode), "Sign challenge for user %s status", pool)
}

func verifySignature(client *broker.ClientWithResponses, challenge string, signature string, statusCode int) {
	body := broker.SignatureRequest{
		Challenge: challenge,
		Signature: signature,
	}
	response, err := client.VerifySignature(context.Background(), body, addBasicAuth("admin", PASSWORD))
	Expect(err).ToNot(HaveOccurred(), "Sign challenge should go through")
	Expect(response.StatusCode).To(Equal(statusCode), "Sign challenge status")
}

func checkBookedServer(pool string, v int, msg string) {
	var bmdList brokerv1.BareMetalDefList
	err := k8sClient.List(context.Background(), &bmdList)
	Expect(err).NotTo(HaveOccurred(), "listing baremetaldefs")
	c := Count(bmdList.Items, func(bmd brokerv1.BareMetalDef) bool {
		return bmd.Spec.PoolDef == pool
	})
	Expect(c).To(Equal(v), "Number of baremetaldef in pool %s - %s", pool, msg)
}

var _ = Describe("API Test", func() {
	It("Create PoolDefs", func() {
		time.Sleep(1000 * time.Millisecond)
		clientApi, _ := broker.NewClientWithResponses(fmt.Sprintf("http://localhost:%d/", BROKER_PORT))
		getPoolUserPassword(clientApi, "user-1", http.StatusOK)
		// Cannot recreate pool a second time
		createPool(clientApi, "user-1", "pool1", nil, http.StatusCreated)
		createPool(clientApi, "user-1", "pool2", Req_u, http.StatusCreated)
		createPool(clientApi, "user-2", "pool3", Req_w, http.StatusCreated)
		createPool(clientApi, "user-3", "pool5", Req_v, http.StatusCreated)
		createPool(clientApi, "user-3", "pool6", Req_b, http.StatusCreated)
		createPool(clientApi, "user-4", "pool7", Req_u, http.StatusCreated)
		// Unknown user (TODO: should also check bad password)
		createPool(clientApi, "user-5", "pool4", nil, http.StatusUnauthorized)
		checkPool(clientApi, "user-1", "pool1")

		ctx := context.Background()
		var poolList brokerv1.PoolDefList
		err := k8sClient.List(ctx, &poolList)
		Expect(err).NotTo(HaveOccurred(), "Listing pool should succeed")
		Expect(len(poolList.Items)).To(Equal(6), "Six pools should be allocated")

		bookServer(clientApi, "user-1", "pool1", "server0", http.StatusCreated)
		bookServer(clientApi, "user-1", "pool1", "server1", http.StatusCreated)
		getAllServer(clientApi, "user-1", "pool1", http.StatusOK)

		bookServer(clientApi, "user-1", "pool1", "server1", http.StatusConflict)
		checkBookedServer("pool1", 2, "2 servers")

		// Do not mess with other people pool
		bookServer(clientApi, "user-2", "pool1", "server3", http.StatusUnauthorized)

		checkBookedServer("pool1", 2, "Still 2 servers after user-2 try")
		bookServer(clientApi, "user-2", "pool3", "server2", http.StatusCreated)
		time.Sleep(1000 * time.Millisecond)
		checkBookedServer("pool3", 1, "simple allocation")
		deleteServer(clientApi, "user-1", "pool1", "server1", http.StatusOK)

		// Do not delete non existing server from pool
		deleteServer(clientApi, "user-1", "pool1", "server5", http.StatusNotFound)

		// Do not delete server from peer user
		deleteServer(clientApi, "user-1", "pool3", "server1", http.StatusUnauthorized)

		checkBookedServer("pool1", 1, "after deletion")
		bookServer(clientApi, "user-1", "pool1", "server3", http.StatusCreated)
		bookServer(clientApi, "user-1", "pool1", "server4", http.StatusCreated)

		// Should fail because we exceed max server for user 1
		bookServer(clientApi, "user-1", "pool1", "server5", http.StatusForbidden)
		bookServer(clientApi, "user-3", "pool5", "server6", http.StatusCreated)

		// Should fail because we exceed user quota for user 3
		bookServer(clientApi, "user-3", "pool5", "server7", http.StatusForbidden)

		bookServer(clientApi, "user-3", "pool6", "server8", http.StatusCreated)
		bookServer(clientApi, "user-3", "pool6", "server9", http.StatusCreated)
		getServer(clientApi, "user-3", "pool6", "server9", http.StatusOK)

		// Should fail because we exceed user quota for user 3
		bookServer(clientApi, "user-3", "pool6", "server10", http.StatusForbidden)

		deleteServer(clientApi, "user-3", "pool6", "server9", http.StatusOK)
		bookServer(clientApi, "user-3", "pool6", "server11", http.StatusCreated)
		getServer(clientApi, "user-3", "pool6", "server11", http.StatusOK)
		getAllServer(clientApi, "user-3", "pool6", http.StatusOK)
		checkBookedServer("pool6", 2, "after recreate")
		deletePool(clientApi, "user-3", "pool6", http.StatusOK)
		checkBookedServer("pool6", 0, "after pool deletion")

		// Should fail because pool6 do not exists
		getServer(clientApi, "user-3", "pool6", "server11", http.StatusUnauthorized)

		deletePool(clientApi, "user-1", "pool1", http.StatusOK)
		checkBookedServer("pool1", 0, "after pool deletion")

		checkBookedServer("pool7", 0, "before server create")
		bookServer(clientApi, "user-4", "pool7", "server20", http.StatusCreated)
		bookServer(clientApi, "user-4", "pool7", "server21", http.StatusCreated)
		bookServer(clientApi, "user-4", "pool7", "server22", http.StatusCreated)
		// Should fail because we exceed maxServers for pool7
		bookServer(clientApi, "user-4", "pool7", "server23", http.StatusForbidden)

		// We only test access to url for challenge management
		// Should fail because no tls file
		signChallenge(clientApi, "user-3", "pool5", "test-challenge", http.StatusNotFound)
		verifySignature(clientApi, "test-challenge", "test-signature", http.StatusNotFound)
	})

})
