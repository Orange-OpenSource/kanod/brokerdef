/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"reflect"
	"sort"
	"time"

	"github.com/go-logr/logr"
	"gitlab.com/Orange-OpenSource/kanod/brokerdef/broker"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/util/retry"

	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	brokerv1 "gitlab.com/Orange-OpenSource/kanod/brokerdef/api/v1"
)

// PoolDefReconciler reconciles a PoolDef object
type PoolDefReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
	Broker *broker.Broker
}

const pooldefFinalizer = "pooldef.kanod.io/finalizer"

//+kubebuilder:rbac:groups=broker.kanod.io,resources=pooldefs,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=broker.kanod.io,resources=pooldefs/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=broker.kanod.io,resources=pooldefs/finalizers,verbs=update

func (r *PoolDefReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("pooldef", req.NamespacedName)
	pooldef := &brokerv1.PoolDef{}
	err := r.Get(ctx, req.NamespacedName, pooldef)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			log.Info("PoolDef resource not found. Ignoring since object must be deleted")
			return ctrl.Result{}, nil
		}
		log.Error(err, "Failed to get PoolDef")
		return ctrl.Result{}, err
	}

	if !pooldef.ObjectMeta.DeletionTimestamp.IsZero() {
		log.Info("launch final deletion with finalizePoolDefDeletion")
		return r.finalizePoolDefDeletion(&ctx, pooldef, log)
	}

	poolUser, err := r.Broker.GetPoolUser(pooldef.Spec.UserName)
	if err != nil {
		log.Error(err, "Failed to get pooluser", "name", pooldef.Spec.UserName)
	}

	pooluserSecret, _ := r.Broker.GetSecret(poolUser.Spec.CredentialName)
	if err != nil || pooluserSecret == nil {
		log.Error(err, "Failed to get secret", "name", poolUser.Spec.CredentialName)
	}

	r.Broker.Mutex.Lock()
	defer r.Broker.Mutex.Unlock()

	poolFound := r.Broker.PoolFound(pooldef.Name)

	poolSpec := broker.Pool{
		Id:       pooldef.Name,
		Username: pooldef.Spec.UserName,
	}

	if poolFound {
		log.Info(fmt.Sprintf("Update pool %s", pooldef.Name))
		r.Broker.UpdatePoolValuesInPoolStore(pooldef.Name, poolSpec)
	} else {
		log.Info(fmt.Sprintf("Create pool in broker %s", pooldef.Name))

		r.Broker.CreateNewPoolInPoolStore(poolSpec)

		if !controllerutil.ContainsFinalizer(pooldef, pooldefFinalizer) {
			controllerutil.AddFinalizer(pooldef, pooldefFinalizer)
			err = r.Update(ctx, pooldef)
			if err != nil {
				return ctrl.Result{}, err
			}
		}
	}

	var netdefs []string
	for _, netdef := range pooldef.Spec.NetDefMap {
		netdefs = append(netdefs, netdef)
	}
	sort.Strings(netdefs)

	status := brokerv1.PoolDefStatus{
		State:              "Created",
		NetworkDefinitions: netdefs,
	}

	poolDef := &brokerv1.PoolDef{}
	err = retry.RetryOnConflict(retry.DefaultRetry,
		func() error {
			if err := r.Get(ctx, req.NamespacedName, poolDef); err != nil {
				return err
			}

			if !reflect.DeepEqual(poolDef.Status, status) {
				poolDef.Status = status
				return r.Status().Update(ctx, poolDef)
			}

			return nil
		})

	if err != nil {
		log.Error(err, "unable to update PoolDef status")
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

// //////////////////////////////////////////////////////////////
func (r *PoolDefReconciler) finalizePoolDefDeletion(
	ctx *context.Context,
	pooldef *brokerv1.PoolDef,
	log logr.Logger,
) (ctrl.Result, error) {

	if controllerutil.ContainsFinalizer(pooldef, pooldefFinalizer) {
		log.Info(fmt.Sprintf("finalize pool deletion for pool %s", pooldef.Name))
		r.Broker.DeletePoolInPoolStore(pooldef.Name)
		controllerutil.RemoveFinalizer(pooldef, pooldefFinalizer)
		err := r.Update(*ctx, pooldef)
		if err != nil {
			return ctrl.Result{}, err
		}
	}
	return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *PoolDefReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&brokerv1.PoolDef{}).
		Complete(r)
}
