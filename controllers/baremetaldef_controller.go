/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"time"

	"github.com/go-logr/logr"
	brokerv1 "gitlab.com/Orange-OpenSource/kanod/brokerdef/api/v1"
	"gitlab.com/Orange-OpenSource/kanod/brokerdef/broker"
	corev1 "k8s.io/api/core/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/util/retry"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

type BareMetalDefReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
	Broker *broker.Broker
}

type Label struct {
	Label string `json:"label"`
}

type NetworkAnnotation struct {
	Itfs  []string          `json:"itfs"`
	Vlans map[string]string `json:"vlans"`
}

const (
	// baremetaldefFinalizer is the name of the finalizer
	baremetaldefFinalizer = "baremetaldef.kanod.io/finalizer"

	// BmhPausedAnnotation is the annotations for pausing baremetalhost reconciliation
	BmhPausedAnnotation = "baremetalhost.metal3.io/paused"

	// BmhPausedAnnotationValue is the value for the annotation for pausing baremetalhost reconciliation
	BmhPausedAnnotationValue = "kanod.io/bmpool"

	// KanodPoolnameAnnotation is the annotation containing the poolname value
	KanodPoolnameAnnotation string = "kanod.io/poolname"

	// KanodServernameAnnotation is the annotation containing the servername value
	KanodServernameAnnotation string = "kanod.io/servername"

	// KanodRedfishSchemaAnnotation is the annotation containing the redfish-schema value
	KanodRedfishSchemaAnnotation string = "kanod.io/redfish-schema"

	MilliResyncPeriod = 5000

	// Field to index on baremetaldefs
	pooldefField = ".spec.poolDef"

	Info  = 0
	Debug = 1
)

//+kubebuilder:rbac:groups=broker.kanod.io,resources=baremetaldefs,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=broker.kanod.io,resources=baremetaldefs/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=broker.kanod.io,resources=baremetaldefs/finalizers,verbs=update
//+kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch
//+kubebuilder:rbac:groups="",resources=pods,verbs=get;list;watch

func (r *BareMetalDefReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("baremetaldef", req.NamespacedName)
	baremetaldef := &brokerv1.BareMetalDef{}
	err := r.Get(ctx, req.NamespacedName, baremetaldef)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			log.Info("BareMetalDef resource not found. Ignoring since object must be deleted")
			return ctrl.Result{}, nil
		}
		log.Error(err, "Failed to get BareMetalDef")
		return ctrl.Result{}, err
	}

	if !baremetaldef.ObjectMeta.DeletionTimestamp.IsZero() {
		return r.finalizeBareMetalDefDeletion(&ctx, baremetaldef, log)
	}

	baremetalSecret := &corev1.Secret{}
	key := client.ObjectKey{
		Namespace: baremetaldef.Namespace,
		Name:      baremetaldef.Spec.BareMetalCredential,
	}
	if err := r.Get(ctx, key, baremetalSecret); err != nil {
		log.Error(err, "unable to get baremetal secret")
		return ctrl.Result{}, err
	}

	r.Broker.Mutex.Lock()
	defer r.Broker.Mutex.Unlock()

	if baremetaldef.Spec.PoolDef == "" {
		if baremetaldef.Spec.NetDefMap != nil {
			baremetaldef.Spec.NetDefMap = nil
			if err := r.Client.Update(ctx, baremetaldef); err != nil {
				log.Error(err, "Cannot reset netDefMap")
				return ctrl.Result{}, err
			}
		}
	} else {
		var poolDef brokerv1.PoolDef
		key := client.ObjectKey{
			Name:      baremetaldef.Spec.PoolDef,
			Namespace: baremetaldef.Namespace}
		if err := r.Client.Get(ctx, key, &poolDef); err != nil {
			log.Error(err, "Cannot access pooldef")
			return ctrl.Result{}, err
		}
		if !reflect.DeepEqual(poolDef.Spec.NetDefMap, baremetaldef.Spec.NetDefMap) {
			log.V(Debug).Info("Update NetDefMap with PoolDef info")
			baremetaldef.Spec.NetDefMap = poolDef.Spec.NetDefMap

			if err := r.Client.Update(ctx, baremetaldef); err != nil {
				log.Error(err, "Cannot update netDefMap")
				return ctrl.Result{}, err
			}
		}

		servername, ok1 := baremetaldef.ObjectMeta.Annotations[KanodServernameAnnotation]
		schema, ok2 := baremetaldef.ObjectMeta.Annotations[KanodRedfishSchemaAnnotation]
		if ok1 && ok2 {
			err = r.Broker.AssociateServerToPool(baremetaldef.Spec.PoolDef, servername, *baremetaldef, schema)
			if err != nil {
				return ctrl.Result{}, err
			}
		} else {
			return ctrl.Result{}, errors.New("missing server or schema annotations in baremetaldef")
		}
	}

	if baremetaldef.Status.State != "Created" {
		baremetaldef.Status.State = "Created"

		baremetaldef := &brokerv1.BareMetalDef{}
		err = retry.RetryOnConflict(retry.DefaultRetry,
			func() error {
				if err := r.Get(ctx, req.NamespacedName, baremetaldef); err != nil {
					return err
				}

				return r.Status().Update(ctx, baremetaldef)
			})

		if err != nil {
			log.Error(err, "unable to update baremetaldef status")
			return ctrl.Result{}, err
		}
	}

	if !controllerutil.ContainsFinalizer(baremetaldef, baremetaldefFinalizer) {
		log.Info(fmt.Sprintf("finalize baremetaldef deletion for pool %s", baremetaldef.Name))
		baremetaldef := &brokerv1.BareMetalDef{}
		err = retry.RetryOnConflict(retry.DefaultRetry,
			func() error {
				if err := r.Get(ctx, req.NamespacedName, baremetaldef); err != nil {
					return err
				}
				controllerutil.AddFinalizer(baremetaldef, baremetaldefFinalizer)

				return r.Update(ctx, baremetaldef)
			})

		if err != nil {
			log.Error(err, "unable to update baremetaldef")
			return ctrl.Result{}, err
		}

	}

	return ctrl.Result{}, nil
}

// //////////////////////////////////////////////////////////////
func (r *BareMetalDefReconciler) finalizeBareMetalDefDeletion(
	ctx *context.Context,
	baremetaldef *brokerv1.BareMetalDef,
	log logr.Logger,
) (ctrl.Result, error) {

	if controllerutil.ContainsFinalizer(baremetaldef, baremetaldefFinalizer) {
		log.Info(fmt.Sprintf("finalize baremetaldef deletion for %s", baremetaldef.Name))
		err := r.Broker.DeleteBareMetal(baremetaldef.Spec.Id, log)
		if err != nil {
			return ctrl.Result{}, err
		}

		controllerutil.RemoveFinalizer(baremetaldef, baremetaldefFinalizer)
		err = r.Update(*ctx, baremetaldef)
		if err != nil {
			return ctrl.Result{}, err
		}
	}
	return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
}

// //////////////////////////////////////////////////////////////
func (r *BareMetalDefReconciler) SetupWithManager(mgr ctrl.Manager) error {
	if err := mgr.GetFieldIndexer().IndexField(context.Background(), &brokerv1.BareMetalDef{}, pooldefField, func(rawObj client.Object) []string {
		bmdef := rawObj.(*brokerv1.BareMetalDef)
		if bmdef.Spec.PoolDef == "" {
			return nil
		}
		return []string{bmdef.Spec.PoolDef}
	}); err != nil {
		return err
	}

	return ctrl.NewControllerManagedBy(mgr).
		For(&brokerv1.BareMetalDef{}).
		Watches(
			&source.Kind{Type: &brokerv1.PoolDef{}},
			handler.EnqueueRequestsFromMapFunc(r.findBMDefFromPoolDef)).
		Complete(r)
}

func (r *BareMetalDefReconciler) findBMDefFromPoolDef(pool client.Object) []reconcile.Request {
	var bmdefList brokerv1.BareMetalDefList
	name := pool.GetName()
	namespace := pool.GetNamespace()
	listOpts := client.ListOptions{
		Namespace:     namespace,
		FieldSelector: fields.OneTermEqualSelector(pooldefField, name),
	}

	err := r.Client.List(context.Background(), &bmdefList, &listOpts)
	if err != nil {
		r.Log.Error(err, "cannot list associated baremetaldef", "pooldef", name, "namespace", namespace)
		return []reconcile.Request{}
	}
	r.Log.Info("Found BMDef for PoolDef modification", "pooldef", name, "namespace", namespace, "number", len(bmdefList.Items))
	requests := make([]reconcile.Request, len(bmdefList.Items))
	for i, item := range bmdefList.Items {
		r.Log.Info("Launch reconcile on BMDef for PoolDef modification", "pooldef", name, "namespace", namespace, "bmdef", item.GetName())
		requests[i] = reconcile.Request{
			NamespacedName: types.NamespacedName{
				Name:      item.GetName(),
				Namespace: item.GetNamespace(),
			},
		}
	}
	return requests
}
