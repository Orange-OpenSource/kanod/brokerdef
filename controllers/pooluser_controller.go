/*
Copyright 2020 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"time"

	"github.com/go-logr/logr"
	brokerv1 "gitlab.com/Orange-OpenSource/kanod/brokerdef/api/v1"
	"gitlab.com/Orange-OpenSource/kanod/brokerdef/broker"
	corev1 "k8s.io/api/core/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/util/retry"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
)

const pooluserFinalizer = "pooluser.kanod.io/finalizer"

// PoolUserReconciler reconciles a PoolUser object
type PoolUserReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
	Broker *broker.Broker
}

//+kubebuilder:rbac:groups=broker.kanod.io,resources=poolusers,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=broker.kanod.io,resources=poolusers/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=broker.kanod.io,resources=poolusers/finalizers,verbs=update

func (r *PoolUserReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("poolUser", req.NamespacedName)
	pooluser := &brokerv1.PoolUser{}
	err := r.Get(ctx, req.NamespacedName, pooluser)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			log.Info("PoolUser resource not found. Ignoring since object must be deleted")
			return ctrl.Result{}, nil
		}
		log.Error(err, "Failed to get PoolUser")
		return ctrl.Result{}, err
	}

	if !pooluser.ObjectMeta.DeletionTimestamp.IsZero() {
		return r.finalizePoolUserDeletion(&ctx, pooluser, log)
	}

	secret := corev1.Secret{}
	key := client.ObjectKey{
		Name:      pooluser.Spec.CredentialName,
		Namespace: pooluser.Namespace,
	}
	err = r.Client.Get(ctx, key, &secret)
	if err != nil {
		r.Log.Error(err, "Cannot access secret for user")
		return ctrl.Result{}, err
	}
	password, ok := secret.Data["password"]
	if !ok {
		return ctrl.Result{}, fmt.Errorf("no password for %s", pooluser.Name)
	}
	user := broker.User{
		Username: pooluser.Name,
		Password: string(password),
	}

	r.Log.Info("Registering user", "user", pooluser.Name)
	r.Broker.AddUserInUserStore(&user)

	if !controllerutil.ContainsFinalizer(pooluser, pooluserFinalizer) {
		pooluser := &brokerv1.PoolUser{}
		err = retry.RetryOnConflict(retry.DefaultRetry,
			func() error {
				if err := r.Get(ctx, req.NamespacedName, pooluser); err != nil {
					return err
				}
				controllerutil.AddFinalizer(pooluser, pooluserFinalizer)
				return r.Update(ctx, pooluser)
			})

		if err != nil {
			log.Error(err, "unable to update pooluser")
			return ctrl.Result{}, err
		}
	}

	return ctrl.Result{}, nil
}

func (r *PoolUserReconciler) finalizePoolUserDeletion(
	ctx *context.Context,
	pooluser *brokerv1.PoolUser,
	log logr.Logger,
) (ctrl.Result, error) {

	if controllerutil.ContainsFinalizer(pooluser, pooluserFinalizer) {
		log.Info(fmt.Sprintf("finalize pooluser deletion for %s", pooluser.Name))
		r.Broker.DeleteUserInPoolStore(pooluser.Name, log)
		controllerutil.RemoveFinalizer(pooluser, pooluserFinalizer)
		err := r.Update(*ctx, pooluser)
		if err != nil {
			return ctrl.Result{}, err
		}
	}
	return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *PoolUserReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&brokerv1.PoolUser{}).
		Complete(r)
}
